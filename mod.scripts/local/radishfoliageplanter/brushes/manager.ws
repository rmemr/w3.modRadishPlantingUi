// ----------------------------------------------------------------------------
class CRadishFoliageBrushList extends CRadishFpUiFilteredList {
    // ------------------------------------------------------------------------
    protected var foliageList: CRadishFoliageList;
    // ------------------------------------------------------------------------
    protected var brushParams: array<SRadFoliageBrushDef>;
    // ------------------------------------------------------------------------
    public function init(foliageList: CRadishFoliageList) {
        this.foliageList = foliageList;
    }
    // ------------------------------------------------------------------------
    private function preprocessBrushParams(brushDef: SRadFoliage_CustomBrushDef) : SRadFoliageBrushDef
    {
        var brush: SRadFoliageBrushDef;
        var minDistance: SRadFoliageMinDistance;
        var scaleRange, rotRange: SRangeF;
        var i, f, typeSlot: int;

        brush = SRadFoliageBrushDef(
            brushDef.brushName,
            brushDef.densityDistance,
        );

        if (brush.densityDistance <= 0.0) {
            brush.densityDistance = 4.0;
        }
        brush.densityDistance = ClampF(brush.densityDistance, 0.05, 100.0);

        brush.typeSelection.Clear();
        for (i = 0; i < brushDef.foliage.Size(); i += 1) {

            typeSlot = foliageList.getSlotId(brushDef.foliage[i].typeId);

            minDistance = SRadFoliageMinDistance(
                brushDef.foliage[i].minDistanceSameType,
                brushDef.foliage[i].minDistanceOtherType,
            );
            scaleRange = SRangeF(
                brushDef.foliage[i].validScaleMin,
                brushDef.foliage[i].validScaleMax,
            );
            rotRange = SRangeF(
                brushDef.foliage[i].validRotMin,
                brushDef.foliage[i].validRotMax,
            );

            // setup default values for minimum distances and ranges
            if (minDistance.sameType == 0)  { minDistance.sameType = 1; }
            if (minDistance.otherType == 0) { minDistance.otherType = minDistance.sameType; }
            minDistance = SRadFoliageMinDistance(
                ClampF(minDistance.sameType, 0.05, 500.0),
                ClampF(minDistance.otherType, 0.05, 500.0)
            );

            if (scaleRange.min == 0 && scaleRange.max == 0) {
                scaleRange = SRangeF(0.9, 1.1);
            } else {
                scaleRange.min = ClampF(scaleRange.min, 0.2, 3);
                scaleRange.max = ClampF(scaleRange.max, scaleRange.min, 3);
            }

            if (rotRange.min == 0 && rotRange.max == 0) {
                rotRange = SRangeF(0, 360);
            } else {
                rotRange.min = ClampF(rotRange.min, 0, 360);
                rotRange.max = ClampF(rotRange.max, rotRange.min, 360);
            }

            // precalculate maxdistance
            brush.maxDistance = MaxF(brush.maxDistance, minDistance.sameType);
            brush.maxDistance = MaxF(brush.maxDistance, minDistance.otherType);

            // prepare prob distribution
            for (f = 0; f < brushDef.foliage[i].weight; f += 1) {
                brush.typeSelection.PushBack(i);
            }

            brush.foliage.PushBack(SRadFoliageParam(
                typeSlot,
                brushDef.foliage[i].weight,
                minDistance,
                scaleRange,
                rotRange
            ));
        }
        return brush;
    }
    // ------------------------------------------------------------------------
    public function addCustomFoliageBrushes(brushes: array<SRadFoliage_CustomBrushDef>) {
        var i, s: int;
        var customCategory: String;

        customCategory = GetLocStringByKeyExt("RADFP_lCustomBrushes");

        s = brushes.Size();
        for (i = 0; i < s; i += 1) {

            // id, caption, CAT1. CAT2, CAT3
            items.PushBack(SModUiCategorizedListItem(
                "CustomBrush_" + IntToString(i),
                brushes[i].brushName,
                customCategory,
                brushes[i].category
            ));

            // preprocessed SRadFoliageBrushDef
            brushParams.PushBack(preprocessBrushParams(brushes[i]));
        }
    }
    // ------------------------------------------------------------------------
    public function addDefaultSingleFoliageBrushes() {
        var i, s: int;
        var foliage: array<SRadFoliageParam>;
        var typeSelection: array<int>;
        var foliageItems: array<SModUiCategorizedListItem>;
        var captionPrefix: String;
        var minDistance: SRangeF;

        foliageItems = foliageList.getItemList();

        captionPrefix = GetLocStringByKeyExt("RADFP_lAutoBrushPrefix");

        s = foliageItems.Size();
        for (i = 0; i < s; i += 1) {

            // id, caption, CAT1. CAT2, CAT3
            items.PushBack(SModUiCategorizedListItem(
                "AutoBrush_" + foliageItems[i].id,
                captionPrefix + " " + foliageItems[i].caption,
                foliageItems[i].cat1,
                foliageItems[i].cat2,
                foliageItems[i].cat3,
            ));

            minDistance = foliageList.getMinDistances(i);
            foliage.Clear();
            foliage.PushBack(SRadFoliageParam(
                i, 1,
                SRadFoliageMinDistance(minDistance.min, minDistance.max),
                SRangeF(0.85, 1.15),
                SRangeF(0, 360)
            ));

            typeSelection.Clear();
            // there is only one foliage slot in this brush and the slot is 0
            typeSelection.PushBack(0);

            // preprocessed SRadFoliageBrushDef
            brushParams.PushBack(SRadFoliageBrushDef(
                foliageItems[i].caption,
                ClampF(minDistance.min * 1.5, 1, 4),
                MaxF(minDistance.min, minDistance.max),
                foliage,
                typeSelection,
            ));
        }
    }
    // ------------------------------------------------------------------------
    public function getBrushParams(strId: String) : SRadFoliageBrushDef {
        var i, s: int;

        for (i = 0; i < items.Size(); i += 1) {
            if (items[i].id == strId) {
                return brushParams[i];
            }
        }
        return SRadFoliageBrushDef("");
    }
    // ------------------------------------------------------------------------
    public function getSelectedBrushParams() : SRadFoliageBrushDef {
        return getBrushParams(selectedId);
    }
    // ------------------------------------------------------------------------
    public function clear() {
        items.Clear();
        brushParams.Clear();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishBrushManager {
    private var brushList: CRadishFoliageBrushList;
    private var foliageList: CRadishFoliageList;
    private var dataLoaded: bool;
    // ------------------------------------------------------------------------
    public function init(foliageList: CRadishFoliageList) {
        this.foliageList = foliageList;
    }
    // ------------------------------------------------------------------------
    private function lazyLoad() {
        brushList = new CRadishFoliageBrushList in this;

        brushList.init(foliageList);

        brushList.clear();
        brushList.addCustomFoliageBrushes(RADFP_getCustomBrushes());

        // add every foliage item as single element brush
        brushList.addDefaultSingleFoliageBrushes();

        brushList.preselect(true);
        dataLoaded = true;
    }
    // ------------------------------------------------------------------------
    public function getBrushList() : CRadishFoliageBrushList {
        if (!dataLoaded) { lazyLoad(); }

        return brushList;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
