// ----------------------------------------------------------------------------
struct SRadFoliageMinDistance {
    var sameType: Float;
    var otherType: Float;
}
// ----------------------------------------------------------------------------
struct SRadFoliageParam {
    var typeSlot: int;
    var weight: byte;
    var minDistance: SRadFoliageMinDistance;
    var scaleRange: SRangeF;
    var rotRange: SRangeF;
}
// ----------------------------------------------------------------------------
struct SRadFoliageBrushDef {
    var brushName: String;
    var densityDistance: float;
    var maxDistance: float;
    var foliage: array<SRadFoliageParam>;

    var typeSelection: array<int>;
}
// ----------------------------------------------------------------------------
