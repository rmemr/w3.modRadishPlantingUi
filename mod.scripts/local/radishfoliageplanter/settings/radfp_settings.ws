// ----------------------------------------------------------------------------
// DO NOT CHANGE PROPERTY ORDER (important for initialization!)
// ----------------------------------------------------------------------------
struct SRadishFpUiConfig {
    var cam: SRadishCamConfig;
    var placement: SRadishPlacementConfig;
}
// ----------------------------------------------------------------------------
class CRadishPlanterConfigData extends IModStorageData {
    // ------------------------------------------------------------------------
    default id = 'RadishPlanterUiConfig';
    // ------------------------------------------------------------------------
    public var config: SRadishFpUiConfig;
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
