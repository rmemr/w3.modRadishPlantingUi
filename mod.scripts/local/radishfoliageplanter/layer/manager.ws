// ----------------------------------------------------------------------------
class CRadishFpUiLayerList extends CRadishFpUiFilteredList {
    // ------------------------------------------------------------------------
    public function setLayerList(layers: array<CRadishFoliageLayer>, showShadowed: bool) : int {
        var l, ls: int;
        var layer: CRadishFoliageLayer;

        items.Clear();

        ls = layers.Size();
        for (l = 0; l < ls; l += 1) {
            layer = layers[l];

            if (showShadowed || !((CEncodedEditableRadishFoliageLayer)layer).isShadowed()) {
                items.PushBack(SModUiCategorizedListItem(
                    layer.getIdString(),
                    layer.getExtendedCaption(),
                ));
            }
        }
        return items.Size();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishFoliageLayerManager {
    private var log: CModLogger;
    // ------------------------------------------------------------------------
    // countre to track revision of data (for lazy sync of cached data in visualizer)
    private var lastUpdate: float;
    // ------------------------------------------------------------------------
    private var worldId: String;
    private var layers: array<CRadishFoliageLayer>;

    private var layerListProvider: CRadishFpUiLayerList;
    private var foliageListProvider: CRadishFoliageList;

    private var selectedLayer: CRadishFoliageLayer;
    // ------------------------------------------------------------------------
    private var showShadowed: bool;
    // ------------------------------------------------------------------------
    public function init(log: CModLogger, foliageList: CRadishFoliageList) {
        this.log = log;
        this.worldId = this.detectWorldId();
        log.debug("foliage layer manager initialized for world [" + this.worldId + "]");

        foliageListProvider = foliageList;

        layers = this.extractEncodedLayers(worldId);

        layerListProvider = new CRadishFpUiLayerList in this;
        refreshListProvider();

        // make sure there is always one layer selected
        if (layers.Size() > 0) {
            layerListProvider.setSelection(layers[0].getIdString(), true);
            selectedLayer = layers[0];
        }
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        var i: int;

        // remove all spawned layer foliage entities
        for (i = 0; i < layers.Size(); i += 1) {
            layers[i].destroy();
        }
        layers.Clear();
    }
    // ------------------------------------------------------------------------
    private function detectWorldId() : String {
        var manager: CCommonMapManager;
        var worldPath: String;
        var currentArea: EAreaName;

        manager = theGame.GetCommonMapManager();
        worldPath = theGame.GetWorld().GetDepotPath();
        currentArea = manager.GetAreaFromWorldPath(worldPath);

        // mapping to encoder defined ids (see repository/worlds)
        switch (currentArea) {
            case AN_NMLandNovigrad:             return "novigrad";
            case AN_Skellige_ArdSkellig:        return "skellige";
            case AN_Kaer_Morhen:                return "kaer_morhen";
            case AN_Prologue_Village:           return "prologue";
            case AN_Wyzima:                     return "vizima";
            case AN_Island_of_Myst:             return "isle_of_mists";
            case AN_Spiral:                     return "spiral";
            case AN_Prologue_Village_Winter:    return "prologue_winter";
            case AN_Velen:                      return "velen";
            //case AN_CombatTestLevel:            return "";
            default:
                return AreaTypeToName(currentArea);
        }
    }
    // ------------------------------------------------------------------------
    private function extractEncodedLayers(worldId: String) : array<CRadishFoliageLayer>
    {
        var dbgLayers: array<CEntity>;
        var dbgLayer: CRadishDbgFoliageLayer;
        var dbgFoliageSets: array<CEntity>;
        var dbgFoliageSet: CRadishDbgFoliageInstanceSet;
        var layers: array<CRadishFoliageLayer>;
        var layer: CRadishFoliageLayer;
        var i, l, s, foliageType : int;

        theGame.GetEntitiesByTag('radish_dbg_foliage_layer', dbgLayers);
        for (l = 0; l < dbgLayers.Size(); l += 1) {
            dbgLayer = (CRadishDbgFoliageLayer)dbgLayers[l];

            if (dbgLayer.isEncoded) {
                layer = new CEncodedRadishFoliageLayer in this;
                layer.init(SRadFoliageLayerId(dbgLayer.layerId), worldId);
                layers.PushBack(layer);
                continue;
            }
            // not encoded means editable -> extract foliage information
            layer = new CEncodedEditableRadishFoliageLayer in this;
            layer.init(SRadFoliageLayerId(dbgLayer.layerId), worldId);
            layer.setFoliageList(foliageListProvider);

            theGame.GetEntitiesByTag(dbgLayer.layerTag, dbgFoliageSets);
            for (i = 0; i < dbgFoliageSets.Size(); i += 1) {
                dbgFoliageSet = (CRadishDbgFoliageInstanceSet)dbgFoliageSets[i];

                if (!((CEncodedEditableRadishFoliageLayer)layer).addFoliageData(dbgFoliageSet)) {
                    log.error("found unknown foliage type [" + dbgFoliageSet.foliageType + "] in layer [" + dbgLayer.layerId + "]");
                    theGame.GetGuiManager().ShowNotification(
                        GetLocStringByKeyExt("RADFP_eInvalidFoliageType")
                        + " " + dbgFoliageSet.foliageType);
                }
            }
            layers.PushBack(layer);
        }
        if (layers.Size() > 0) {
            log.info("found encoded foliage layers (in [" + worldId + "]) to manage: " + IntToString(layers.Size()));
        } else {
            log.info("no encoded foliage layers found.");
        }
        return layers;
    }
    // ------------------------------------------------------------------------
    public function refreshListProvider() {
        layerListProvider.setLayerList(this.layers, this.showShadowed);
    }
    // ------------------------------------------------------------------------
    public function getLayerList() : CRadishFpUiLayerList {
        return this.layerListProvider;
    }
    // ------------------------------------------------------------------------
    public function getSelected() : CRadishFoliageLayer {
        return selectedLayer;
    }
    // ------------------------------------------------------------------------
    public function selectLayer(layerIdString: String) : CRadishFoliageLayer {
        var null: CRadishFoliageLayer;
        var i, s: int;
        s = layers.Size();

        for (i = 0; i < s; i += 1) {
            if (layers[i].getIdString() == layerIdString) {
                selectedLayer = layers[i];
                return selectedLayer;
            }
        }
        selectedLayer = null;
        return null;
    }
    // ------------------------------------------------------------------------
    // checks whether name is unique for all non-encoded (!) layers, ignoring
    // context.
    public function verifyLayerId(newId: SRadFoliageLayerId) : bool {
        var encoded: CEncodedRadishFoliageLayer;
        var i, s: int;
        var tmpId: SRadFoliageLayerId;
        var newLayerName: String;

        newLayerName = RadFpUi_escapeAsId(newId.layerName);

        s = layers.Size();
        for (i = 0; i < s; i += 1) {
            tmpId = layers[i].getId();
            encoded = (CEncodedRadishFoliageLayer)layers[i];
            //TODO editable?
            if (!encoded && tmpId.layerName == newLayerName) {
                return false;
            }
        }
        return true;
    }
    // ------------------------------------------------------------------------
    // layer management
    // ------------------------------------------------------------------------
    private function generateUniqueLayerId(baseId: SRadFoliageLayerId) : SRadFoliageLayerId {
        var newId: SRadFoliageLayerId;
        var i: int;

        newId = baseId;
        while (!verifyLayerId(newId)) {
            i += 1;
            newId.layerName = baseId.layerName + IntToString(i);
        }
        return newId;
    }
    // ------------------------------------------------------------------------
    public function addNew() : CRadishFoliageLayer {
        var layer: CRadishFoliageLayer;
        var newId: SRadFoliageLayerId;

        newId = generateUniqueLayerId(SRadFoliageLayerId("new layer",));

        layer = new CRadishFoliageLayer in this;
        layer.init(newId, worldId);
        layer.setFoliageList(foliageListProvider);
        log.debug("created new layer: " + newId.layerName);

        layers.PushBack(layer);

        refreshListProvider();
        layerListProvider.setSelection(layer.getIdString(), true);
        selectedLayer = layer;

        //this.lastUpdate = theGame.GetEngineTimeAsSeconds();
        return selectedLayer;
    }
    // ------------------------------------------------------------------------
    private function deleteEditableById(id: SRadFoliageLayerId) {
        var tmpLayers: array<CRadishFoliageLayer>;
        var i, s: int;

        s = layers.Size();
        for (i = 0; i < s; i += 1) {
            if (layers[i].getId() == id && layers[i].isDeleteable()) {
                layers[i].destroy();
            } else {
                tmpLayers.PushBack(layers[i]);
            }
        }
        layers = tmpLayers;
    }
    // ------------------------------------------------------------------------
    public function cloneSelected() : bool {
        var layer: CRadishFoliageLayer;
        var newId: SRadFoliageLayerId;

        if (selectedLayer) {
            if (!selectedLayer.isCloneable()) {
                // not possible to clone encoded -> error
                log.error("not possible to clone encoded layer!");
                return false;
            }
            if ((CEncodedEditableRadishFoliageLayer)selectedLayer) {
                // "hide" original non-deleteable layer in list and all proxies
                ((CEncodedEditableRadishFoliageLayer)selectedLayer).setShadowed(true);

                // use identical id
                newId = selectedLayer.getId();
                newId.encoded = false;

                // pre definition cloning encoded layer will overwrite previously
                // cloned layer!
                // -> remove any non encoded layer with this id
                deleteEditableById(newId);
            } else {
                newId = generateUniqueLayerId(selectedLayer.getId());
            }
            layer = new CRadishFoliageLayer in this;
            layer.init(newId, worldId);
            layer.setFoliageList(foliageListProvider);
            layer.cloneFrom(selectedLayer);
            log.debug("cloned layer: " + layer.getName());

            layers.PushBack(layer);
            refreshListProvider();
            layerListProvider.setSelection(layer.getIdString(), true);
            selectedLayer = layer;
        }

        //this.lastUpdate = theGame.GetEngineTimeAsSeconds();
        return true;
    }
    // ------------------------------------------------------------------------
    public function deleteSelected() : bool {
        if (selectedLayer && selectedLayer.isDeleteable()) {
            log.debug("deleting layer: " + selectedLayer.getName());

            if (layers.Remove(selectedLayer)) {
                selectedLayer.destroy();
                delete selectedLayer;

                refreshListProvider();

                if (layers.Size() > 0) {
                    layerListProvider.setSelection(layers[0].getIdString(), true);
                    selectedLayer = layers[0];
                }
                layerListProvider.preselect(true);
                //this.lastUpdate = theGame.GetEngineTimeAsSeconds();
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------------------
    public function existsEditableLayer(id: SRadFoliageLayerId) : bool {
        var i, s: int;
        id.encoded = false;
        id.readonly = false;

        s = layers.Size();
        for (i = 0; i < s; i += 1) {
            if (layers[i].getId() == id) {
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------------------
    public function unshadowEncodedLayer(id: SRadFoliageLayerId) {
        var i, s: int;

        id.encoded = true;
        id.readonly = false;

        s = layers.Size();
        for (i = 0; i < s; i += 1) {
            if (layers[i].getId() == id) {
                ((CEncodedEditableRadishFoliageLayer)layers[i]).setShadowed(false);
                refreshListProvider();

                //this.lastUpdate = theGame.GetEngineTimeAsSeconds();
                break;
            }
        }
    }
    // ------------------------------------------------------------------------
    public function toggleShadowedVisibility() : bool {
        showShadowed = !showShadowed;
        refreshListProvider();
        return showShadowed;
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function logDefinition(optional isAutoLogged: bool) {
        var definitionWriter: CRadishDefinitionWriter;
        var defs, root: SEncValue;
        var i: int;
        var id: SRadFoliageLayerId;

        root = SEncValue(EEVT_Map);
        defs = SEncValue(EEVT_Map);

        for (i = 0; i < layers.Size(); i += 1) {
            encMapPush(layers[i].getIdString(), layers[i].asDefinition(), defs);
        }
        root.m.PushBack(SEncKeyValue("foliage", defs));

        definitionWriter = new CRadishDefinitionWriter in this;
        if (isAutoLogged) {
            definitionWriter.create('W3FOLIAGE', "Radish Foliage Planter UI", root);
        } else {
            definitionWriter.create('W3FOLIAGE', "Radish Foliage Planter UI (auto-log)", root);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
