// ----------------------------------------------------------------------------
struct SRadishDbgFoliage {
    var pos: Vector;
    var rot: Float;
    var scale: Float;
}
// ----------------------------------------------------------------------------
class CRadishDbgFoliageLayer extends CEntity {
    public var layerId: String;
    public var layerTag: CName;
    public var isEncoded: bool;
}
// ----------------------------------------------------------------------------
class CRadishDbgFoliageInstanceSet extends CEntity {
    public var layerId: String;
    public var foliageType: String;
    public var instances: array<SRadishDbgFoliage>;
}
// ----------------------------------------------------------------------------
class CEncodedRadishFoliageLayer extends CRadishFoliageLayer {
    // ------------------------------------------------------------------------
    public function init(id: SRadFoliageLayerId, worldId: String) {
        id.encoded = true;
        id.readonly = true;
        super.init(id, worldId);
    }
    // ------------------------------------------------------------------------
    public function getIdString() : String {
        return super.getIdString() + ":ro";
    }
    // ------------------------------------------------------------------------
    public function getExtendedCaption() : String {
        var prefix: String;
        var suffix: String;

        prefix = "ENC:<font color=\"#995555\">";
        suffix = "</font>";
        return prefix + super.getExtendedCaption() + suffix;
    }
    // ------------------------------------------------------------------------
    public function show(doShow: bool) {
        visibility = true;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CEncodedEditableRadishFoliageLayer extends CRadishFoliageLayer {
    // ------------------------------------------------------------------------
    private var shadowed: bool;
    // ------------------------------------------------------------------------
    public function init(id: SRadFoliageLayerId, worldId: String) {
        id.encoded = true;
        id.readonly = false;
        // visible by default
        this.visibility = true;
        super.init(id, worldId);
    }
    // ------------------------------------------------------------------------
    public function getIdString() : String {
        return super.getIdString() + ":e";
    }
    // ------------------------------------------------------------------------
    public function addFoliageData(data: CRadishDbgFoliageInstanceSet) : bool {
        var foliageTypeMeta: SRadFoliageProxyInfo;
        var foliage: SRadishDbgFoliage;
        var entity: CRadishFoliageEntity;
        var f, s, foliageType : int;

        foliageType = foliageList.getSlotId(data.foliageType);
        foliageTypeMeta = foliageList.getMetaInfo(foliageType);
        if (foliageType > 0) {
            s = data.instances.Size();
            for (f = 0; f < s; f += 1) {
                foliage = data.instances[f];

                // new foliage -> spawn
                entity = new CRadishFoliageEntity in this;
                entity.init(
                    data.foliageType,
                    foliageTypeMeta,
                    foliage.pos, foliage.rot, foliage.scale);

                proxies.PushBack(entity);
                type.PushBack(foliageType);
                pos.PushBack(foliage.pos);
                rot.PushBack(foliage.rot);
                scale.PushBack(foliage.scale);
            }
            return true;
        } else {
            return false;
        }
    }
    // ------------------------------------------------------------------------
    // naming
    // ------------------------------------------------------------------------
    public function getExtendedCaption() : String {
        var prefix: String;
        var suffix: String;

        if (shadowed) {
            prefix = "edit:<font color=\"#995555\">";
        } else {
            prefix = "edit:<font color=\"#999999\">";
        }
        suffix = "</font>";
        return prefix + super.getExtendedCaption() + suffix;
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    public function setShadowed(shadowed: bool) {
        if (shadowed) {
            // hide visual proxies
            show(false);
        }
        this.shadowed = shadowed;
    }
    // ------------------------------------------------------------------------
    public function isShadowed() : bool {
        return shadowed;
    }
    // ------------------------------------------------------------------------
    public function asDefinition() : SEncValue {
        var content, foliageInstance: SEncValue;

        content = super.asDefinition();

        if (getItemCount() > 0) {
            content.m.Insert(0, SEncKeyValue("#", StrToEncValue("PREVIOUSLY ENCODED AND UNCHANGED DATA")));
        }

        return content;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
