// ----------------------------------------------------------------------------
class CRadishFoliageEntity {
    // ------------------------------------------------------------------------
    private var foliage: CEntity;
    private var wireframe: CEntity;
    private var wireframeMesh: CMeshComponent;

    private var foliageTemplate: CEntityTemplate;
    private var currentTemplateScale: int;
    // ------------------------------------------------------------------------
    private var foliageId: String;
    private var meta: SRadFoliageProxyInfo;
    private var pos: Vector;
    private var rot: Float;
    private var scale: Float;
    // ------------------------------------------------------------------------
    public function init(
        foliageId: String,
        meta: SRadFoliageProxyInfo,
        pos: Vector,
        rotation: float,
        scale: float)
    {
        var template: CEntityTemplate;

        this.foliageId = foliageId;
        this.meta = meta;
        this.pos = pos;
        this.rot = rotation;
        this.scale = -1;

        template = (CEntityTemplate)LoadResource(
            "dlc\modtemplates\radishplanterui\base\wireframe.w2ent", true);
        wireframe = theGame.CreateEntity(
            template, pos, EulerAngles(0.0, rotation, 0.0));
        wireframe.AddTag('RADFP_FOLIAGE');
        wireframe.SetHideInGame(true);

        wireframeMesh = (CMeshComponent)wireframe.GetComponentByClassName('CMeshComponent');
        wireframeMesh.SetCastingShadows(false);

        update(pos, rotation, scale);
    }
    // ------------------------------------------------------------------------
    protected function calculateTemplateScale(newScale: float) : int {
        var i, targetScale, s: int;

        // apply approximate foliage appearance
        targetScale = RoundF(newScale * 100);

        s = meta.mips.Size() - 1;
        for (i = s; i >= 0; i -= 1) {
            if (meta.mips[i] <= targetScale) {
                return meta.mips[i];
            }
        }
        // fallback: smallest available mip level
        return meta.mips[0];
    }
    // ------------------------------------------------------------------------
    public function getWorldPosition() : Vector {
        return pos;
    }
    // ------------------------------------------------------------------------
    public function getWorldRotation() : float {
        return rot;
    }
    // ------------------------------------------------------------------------
    public function moveTo(newPos: Vector, newRotation: Float) {
        // move + rotate wireframe
        wireframe.TeleportWithRotation(newPos, EulerAngles(0, newRotation, 0));
        foliage.TeleportWithRotation(newPos, EulerAngles(0, newRotation, 0));
        pos = newPos;
        rot = newRotation;
    }
    // ------------------------------------------------------------------------
    private function respawnFoliage() {
        foliage.Destroy();
        foliage = theGame.CreateEntity(
            foliageTemplate, pos, EulerAngles(0.0, rot, 0.0));
        foliage.AddTag('RADFP_FOLIAGE');

        wireframe.TeleportWithRotation(pos, EulerAngles(0, rot, 0));
    }
    // ------------------------------------------------------------------------
    public function update(newPos: Vector, newRot: Float, newScale: Float) {
        var newSize, newMin, meshPos: Vector;
        var newTemplateScale: int;

        newTemplateScale = calculateTemplateScale(newScale);

        if (currentTemplateScale != newTemplateScale) {
            currentTemplateScale = newTemplateScale;
            pos = newPos;
            rot = newRot;

            // respan with different template required
            foliageTemplate = (CEntityTemplate)LoadResource(
                "dlc/modtemplates/radishplanterui/foliage/"
                    + foliageId + "_" + IntToString(currentTemplateScale) + ".w2ent", true);

            respawnFoliage();
        } else {
            if (pos != newPos || rot != newRot) {
                pos = newPos;
                rot = newRot;
                // respawn same template
                respawnFoliage();
            }
            // otherwise only scale changed - but not enough to require a
            // different foliage template
        }

        if (scale != newScale) {
            scale = newScale;
            // rescale wireframe exactly (it is already at the correct position)

            newSize = meta.size * scale;
            newMin = meta.min * scale;

            // the dimension saved in the srt files seem to use a different
            // coordinate system orientation
            meshPos = newMin + 0.5 * newSize;
            meshPos.X *= -1.0;

            wireframeMesh.SetScale(newSize);
            wireframeMesh.SetPosition(meshPos);
        }
    }
    // ------------------------------------------------------------------------
    public function showWireframe() {
        wireframe.SetHideInGame(false);
    }
    // ------------------------------------------------------------------------
    public function hideWireframe() {
        wireframe.SetHideInGame(true);
    }
    // ------------------------------------------------------------------------
    public function hideFoliage() {
        var null: CEntity;
        if (foliage) {
            foliage.Destroy();
        }
        foliage = null;
    }
    // ------------------------------------------------------------------------
    public function showFoliage() {
        // has to be respawned in new location
        respawnFoliage();
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        foliage.Destroy();
        wireframe.Destroy();
    }
    // ------------------------------------------------------------------------
    public function show(doShow: bool) {
        if (doShow) {
            if (!foliage) {
                respawnFoliage();
            }
        } else {
            hideFoliage();
            wireframe.SetHideInGame(true);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
