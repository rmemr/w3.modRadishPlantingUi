// ----------------------------------------------------------------------------
class CRadishCachingTerrainProbe extends CRadishTerrainProbe {
    // ------------------------------------------------------------------------
    private var originOffset: int;
    private var stripeCount: int;
    private var tilesPerStripeCount: int;
    private var cachedStripes: array<CRadTerrainHeightStripe>;
    // ------------------------------------------------------------------------
    private var cacheQueue: array<CRadTerrainHeightTile>;
    // ------------------------------------------------------------------------
    public function init() {
        var stripe: CRadTerrainHeightStripe;
        var terrainSize: float;
        var i, _: int;

        if (theGame.GetWorld().GetTerrainParameters(terrainSize, _)) {
            tilesPerStripeCount = CeilF(terrainSize / 100.0);
        } else {
            // data for biggest vanilla map (novigrad)
            tilesPerStripeCount = CeilF(8625.0 / 100.0);
        }

        originOffset = -tilesPerStripeCount * 100 / 2;
        stripeCount = tilesPerStripeCount;

        for (i = 0; i < stripeCount; i += 1) {
            stripe = new CRadTerrainHeightStripe in this;
            // Y-stripes, tiles are always 100x100 -> Y-offset i * 100
            stripe.initParameters(tilesPerStripeCount, originOffset, originOffset + i * 100);

            cachedStripes.PushBack(stripe);
        }
    }
    // ------------------------------------------------------------------------
    public function precacheFor(pos: Vector, yaw: float) {
        var tileX, tileY: int;
        var projectedPos: Vector;

        // cache only a couple of tiles in direction of current camera
        projectedPos = pos + 75.0 * VecFromHeading(yaw - 45.0);
        tileX = FloorF((projectedPos.X - originOffset) / 100.0);
        tileY = FloorF((projectedPos.Y - originOffset) / 100.0);
        createNewTile(tileX, tileY);

        projectedPos = pos + 75.0 * VecFromHeading(yaw - 0.0);
        tileX = FloorF((projectedPos.X - originOffset) / 100.0);
        tileY = FloorF((projectedPos.Y - originOffset) / 100.0);
        createNewTile(tileX, tileY);

        projectedPos = pos + 75.0 * VecFromHeading(yaw + 45.0);
        tileX = FloorF((projectedPos.X - originOffset) / 100.0);
        tileY = FloorF((projectedPos.Y - originOffset) / 100.0);
        createNewTile(tileX, tileY);

        // make sure the tile for exact position is also cached
        tileX = FloorF((pos.X - originOffset) / 100.0);
        tileY = FloorF((pos.Y - originOffset) / 100.0);

        createNewTile(tileX, tileY);
    }
    // ------------------------------------------------------------------------
    private function createNewTile(x: int, y: int) {
        var tile: CRadTerrainHeightTile;

        if (x < 0 || x >= tilesPerStripeCount || y < 0 || y >= stripeCount) {
            LogChannel('DEBUG', "ignoring tile creation for ("
                + IntToString(x) + ", " + IntToString(y) + ")");
            return;
        }

        // check if tile already created and cached
        if (!cachedStripes[y].isInitialized()) {
            //LogChannel('DEBUG', "initializing stripe " + IntToString(y));
            cachedStripes[y].initTiles();
        }

        tile = cachedStripes[y].tile(x);

        //if (tile.isDone()) {
        //    LogChannel('DEBUG', "tile already processed ("
        //        + IntToString(x) + ", " + IntToString(y) + ")");
        //}
        //if (cacheQueue.Contains(tile)) {
        //    LogChannel('DEBUG', "tile already in cache queue ("
        //        + IntToString(x) + ", " + IntToString(y) + ")");
        //}

        if (!tile.isDone() && !cacheQueue.Contains(tile)) {
            //LogChannel('DEBUG', "adding tile ("
            //    + IntToString(x) + ", " + IntToString(y) + ") to cacheQueue");
            cacheQueue.PushBack(tile);
        }
    }
    // ------------------------------------------------------------------------
    public function needsCaching() : bool {
        return cacheQueue.Size() > 0;
    }
    // ------------------------------------------------------------------------
    public function getNextToBeCachedPos(out pos: Vector) : bool {
        var tile: CRadTerrainHeightTile;
        var failed: int;

        while (cacheQueue.Size() > 0) {
            tile = cacheQueue.Last();

            if (!tile.isDone()) {
                if (tile.getNextToBeCachedPos(pos)) {
                    return true;
                } else {
                    failed = tile.getFailedCount();
                    if (failed > 0) {
                        theGame.GetGuiManager().ShowNotification(
                            GetLocStringByKeyExt("RADFP_eFailedCachingPoints") + IntToString(failed)
                        );
                    }
                }
            }
            // tile is now done
            cacheQueue.PopBack();
        }
        return false;
    }
    // ------------------------------------------------------------------------
    public function getApproximatedGroundPos(
        pos: Vector, out groundPos: Vector) : bool
    {
        var pos2, groundNormal: Vector;
        var tileX, tileY: int;
        var tile: CRadTerrainHeightTile;

        pos.W = 1.0;
        pos2 = pos;
        pos.Z = 5000;
        pos2.Z = -5000;

        if (theGame.GetWorld().StaticTrace(pos, pos2, groundPos, groundNormal)) {
            return true;
        } else {
            // calculate to be cached tiles for pos + yaw
            tileX = FloorF((pos.X - originOffset) / 100.0);
            tileY = FloorF((pos.Y - originOffset) / 100.0);

            tile = cachedStripes[tileY].tile(tileX);
            if (tile.getGroundPos(pos, groundPos)) {
                return true;
            } else {
                return false;
            }
        }
    }
    // ------------------------------------------------------------------------
    public function getExactGroundPos(pos: Vector, out groundPos: Vector) : bool {
        var pos2, groundNormal: Vector;

        pos.W = 1.0;
        pos2 = pos;
        pos.Z = 5000;
        pos2.Z = -5000;

        if (theGame.GetWorld().StaticTrace(pos, pos2, groundPos, groundNormal)) {
            return true;
        } else {
            return false;
        }
    }
    // ------------------------------------------------------------------------
    public function refreshTerrainCollision(target: Vector) : bool {
        var tmp, groundPos, groundNormal: Vector;

        target.Z = 3000;
        target.W = 1.0;
        thePlayer.Teleport(target);

        tmp = target;
        tmp.Z = -3000;

        if (theGame.GetWorld().StaticTrace(tmp, target, groundPos, groundNormal)) {
            thePlayer.Teleport(groundPos);
            return true;
        } else {
            LogChannel('DEBUG', "terrainProbe.refreshTerrainCollision failed...");
            return false;
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
