// ----------------------------------------------------------------------------
state RadFp_LayerOverview in CRadishFoliagePlanterUi {
    // ------------------------------------------------------------------------
    private var searchFilterInput: bool;
    // ------------------------------------------------------------------------
    private var selectedLayer: CRadishFoliageLayer;
    private var layerManager: CRadishFoliageLayerManager;
    private var listProvider: CRadishFpUiLayerList;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.currentModeId = "LayerOverview";
        parent.workContext = 'MOD_PlantingUi';

        this.layerManager = parent.layerManager;
        layerManager.refreshListProvider();
        listProvider = layerManager.getLayerList();
        selectedLayer = layerManager.getSelected();

        theInput.StoreContext(parent.workContext);
        registerListeners();

        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        if (prevStateName == 'RadFp_Sleeping') {
            listProvider.preselect(true);
        }

        updateFpCaption();
        parent.showUi(true);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        unregisterListeners();
    }
    // ------------------------------------------------------------------------
    event OnInteractiveTime(action: SInputAction) {
        if (IsReleased(action)) {
            parent.PushState('RadFp_InteractiveTime');
        }
    }
    // ------------------------------------------------------------------------
    event OnInteractiveCam(action: SInputAction) {
        if (IsReleased(action)) {
            parent.showUi(false);
            parent.PushState('RadFp_InteractiveCamera');
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {

        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ResetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ToggleUi'));

        hotkeyList.PushBack(HotkeyHelp_from('RADFP_RenameLayer'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_AddLayer'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_DelLayer'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SelectPrevNext'));
        //hotkeyList.PushBack(HotkeyHelp_from('RADFP_SelectNext', "RADFP_NextLayer"));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ToggleShadowedLayerVisibility'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_CloneLayer'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_UnshadowLayer'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ToggleVisibility'));

        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ToggleInteractiveCam', 'RAD_ToggleInteractiveCam', IK_LControl));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_AdjustTime', 'RAD_ToggleInteractiveTime'));

        hotkeyList.PushBack(HotkeyHelp_from('RADFP_LogDefinition'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Minimize'));
        parent.OnHotkeyHelp(hotkeyList);
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Quit'));
    }
    // ------------------------------------------------------------------------
    event OnEditLayer(action: SInputAction) {
       // prevent direct jump to first selected category by checking "released"
       // and checking "pressed" in substate
        if (IsReleased(action)) {
            if (selectedLayer.isEditable()) {
                parent.PushState('RadFp_FoliageEditing');
            } else {
                parent.error(GetLocStringByKeyExt("RADFP_eNonEditableLayer"));
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnAddLayer(action: SInputAction) {
        if (IsReleased(action)) {
            selectedLayer = layerManager.addNew();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnDelLayer(action: SInputAction) {
        var msgTitle, msgText: String;

        if (IsReleased(action)) {
            if (selectedLayer.isDeleteable()) {
                if (parent.confirmPopup) { delete parent.confirmPopup; }

                parent.confirmPopup = new CModUiActionConfirmation in this;
                msgTitle = GetLocStringByKeyExt("RADFP_tLayerOverviewConfirmPopup");
                msgText = GetLocStringByKeyExt("RADFP_mLayerDelete") + "\"" + selectedLayer.getCaption() + "\"?";

                parent.confirmPopup.open(
                    parent.popupCallback, msgTitle, msgText, "deleteLayer");
            } else {
                parent.error(GetLocStringByKeyExt("RADFP_eEncLayerDelete"));
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnDeleteConfirmed() {
        if (!layerManager.deleteSelected()) {
            parent.error(GetLocStringByKeyExt("RADFP_eDeleteFailed"));
        }
        selectedLayer = layerManager.getSelected();
        updateView();
        parent.showUi(true);
    }
    // ------------------------------------------------------------------------
    event OnClone(action: SInputAction) {
        var msgTitle, msgText: String;

        if (IsReleased(action) && selectedLayer.isCloneable()) {
            if (layerManager.existsEditableLayer(selectedLayer.getId())) {
                if (parent.confirmPopup) { delete parent.confirmPopup; }

                parent.confirmPopup = new CModUiActionConfirmation in this;
                msgTitle = GetLocStringByKeyExt("RADFP_tLayerOverviewConfirmPopup");
                msgText = GetLocStringByKeyExt("RADFP_mLayerOverwrite") + " \"" + selectedLayer.getCaption() + "\"?";

                parent.confirmPopup.open(
                    parent.popupCallback, msgTitle, msgText, "cloneLayer");
            } else {
                OnCloneConfirmed();
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnCloneConfirmed() {
        var msgId: String;

        if ((CEncodedEditableRadishFoliageLayer)selectedLayer) {
            msgId = GetLocStringByKeyExt("RADFP_iClonedEncodedLayer");
        } else {
            msgId = GetLocStringByKeyExt("RADFP_iClonedLayer");
        }
        if (layerManager.cloneSelected()) {
            selectedLayer = layerManager.getSelected();
            parent.notice(msgId);

            updateView();
            parent.showUi(true);
        } else {
            parent.error(GetLocStringByKeyExt("RADFP_eCloneLayerFailed"));
        }
    }
    // ------------------------------------------------------------------------
    event OnUnShadowLayer(action: SInputAction) {
        if (IsPressed(action)) {
            if (layerManager.existsEditableLayer(selectedLayer.getId())) {
                parent.error(GetLocStringByKeyExt("RADFP_eUnshadowedIdConflict"));
            } else {
                layerManager.unshadowEncodedLayer(selectedLayer.getId());
                updateView();
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleShadowedVisibility(action: SInputAction) {
        if (IsPressed(action)) {
            if (layerManager.toggleShadowedVisibility()) {
                parent.notice(GetLocStringByKeyExt("RADFP_iShadowedLayerVisible"));
            } else {
                parent.notice(GetLocStringByKeyExt("RADFP_iShadowedLayerHidden"));
            }
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleVisibility(action: SInputAction) {
        if (IsPressed(action)) {
            selectedLayer.toggleVisibility();
            layerManager.refreshListProvider();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnRename(action: SInputAction) {
        if (selectedLayer.isRenameable() && !parent.view.listMenuRef.isEditActive() && IsPressed(action)) {
            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("RADFP_lLayerRename"),
                layerManager.getSelected().getCaption());
        }
    }
    // ------------------------------------------------------------------------
    // called by user action to start filter input
    event OnFilter(action: SInputAction) {
        if (!parent.view.listMenuRef.isEditActive() && IsPressed(action)) {
            searchFilterInput = true;
            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("RADFP_lFilter"), listProvider.getWildcardFilter());
        }
    }
    // ------------------------------------------------------------------------
    // called by user action to reset currently set filter
    event OnResetFilter() {
        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        updateView();
        searchFilterInput = false;
    }
    // ------------------------------------------------------------------------
    event OnInputEnd(inputString: String) {
        var newId: SRadFoliageLayerId;

        if (searchFilterInput) {
            if (inputString == "") {
                OnResetFilter();
            } else {
                // Note: filter field is not removed to indicate the current filter
                listProvider.setWildcardFilter(inputString);
                updateView();
            }
            searchFilterInput = false;
        } else {
            if (StrLen(inputString) > 2) {
                selectedLayer = layerManager.getSelected();
                newId = selectedLayer.getId();
                newId.layerName = inputString;

                if (layerManager.verifyLayerId(newId)) {
                    selectedLayer.setId(newId);
                    layerManager.refreshListProvider();
                    listProvider.setSelection(selectedLayer.getIdString(), true);
                } else {
                    parent.error(GetLocStringByKeyExt("RADFP_iUniqueLayerName"));
                }
            } else {
                parent.error(GetLocStringByKeyExt("RADFP_eInvalidLayerName"));
            }
            parent.view.listMenuRef.resetEditField();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnInputCancel() {
        if (searchFilterInput) {
            searchFilterInput = false;
            parent.notice(GetLocStringByKeyExt("UI_CanceledSearch"));
        } else {
            parent.notice(GetLocStringByKeyExt("UI_CanceledEdit"));
        }
        parent.view.listMenuRef.resetEditField();
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnCycleSelection(action: SInputAction) {
        var prev: bool;
        var id: String;

        if (action.value != 0) {
            prev = action.value < 0;

            if (prev) {
                id = listProvider.getPreviousId();
            } else {
                id = listProvider.getNextId();
            }
            OnSelected(id);
        }
    }
    // ------------------------------------------------------------------------
    event OnUpdateView() {
        var wildcard: String;
        // Note: if search filter is active show the wildcard to indicate the
        // current filter
        wildcard = listProvider.getWildcardFilter();
        if (wildcard != "") {
            parent.view.listMenuRef.setInputFieldData(
                GetLocStringByKeyExt("RADFP_lFilter"), wildcard);
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnSelected(listItemId: String) {
        // listprovider opens a category if a category was selected otherwise
        // returns true (meaning a "real" item was selected)
        if (listProvider.setSelection(listItemId, true)) {
            parent.layerManager.selectLayer(listItemId);
            selectedLayer = parent.layerManager.getSelected();
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    event OnLogLayerData(action: SInputAction) {
        if (IsPressed(action)) {
            parent.layerManager.logDefinition();
            parent.notice(GetLocStringByKeyExt("RADFP_iDefinitionLogged"));
        }
    }
    // ------------------------------------------------------------------------
    protected function updateView() {
        // set updated list data and render in listview
        parent.view.listMenuRef.setListData(
            listProvider.getFilteredList(),
            listProvider.getMatchingItemCount(),
            // number of items without filtering
            listProvider.getTotalCount());

        parent.view.listMenuRef.updateView();
    }
    // ------------------------------------------------------------------------
    private function updateFpCaption() {
        parent.view.title = GetLocStringByKeyExt("RADFP_lListOverviewTitle");
        parent.view.statsLabel = GetLocStringByKeyExt("RADFP_lListElements");
        // update field if the menu is already open
        parent.view.listMenuRef.setTitle(GetLocStringByKeyExt("RADFP_lListOverviewTitle"));
        parent.view.listMenuRef.setStatsLabel(GetLocStringByKeyExt("RADFP_lListElements"));
    }
    // ------------------------------------------------------------------------
    event OnToggleUi(action: SInputAction) {
        if (IsPressed(action)) {
            parent.toggleUi();
        }
    }
    // ------------------------------------------------------------------------
    private function registerListeners() {
        parent.registerListeners();

        theInput.RegisterListener(parent, 'OnQuitRequest', 'RADFP_Quit');

        theInput.RegisterListener(this, 'OnAddLayer', 'RADFP_AddLayer');
        theInput.RegisterListener(this, 'OnDelLayer', 'RADFP_DelLayer');
        theInput.RegisterListener(this, 'OnEditLayer', 'RADFP_EditLayer');
        theInput.RegisterListener(this, 'OnClone', 'RADFP_CloneLayer');
        theInput.RegisterListener(this, 'OnUnShadowLayer', 'RADFP_UnshadowLayer');
        theInput.RegisterListener(this, 'OnToggleVisibility', 'RADFP_ToggleVisibility');
        theInput.RegisterListener(this, 'OnToggleShadowedVisibility', 'RADFP_ToggleShadowedLayerVisibility');

        theInput.RegisterListener(this, 'OnRename', 'RADFP_RenameLayer');

        theInput.RegisterListener(this, 'OnToggleUi', 'RADFP_ToggleUi');
        theInput.RegisterListener(this, 'OnFilter', 'RADFP_SetFilter');
        theInput.RegisterListener(this, 'OnResetFilter', 'RADFP_ResetFilter');

        theInput.RegisterListener(this, 'OnCycleSelection', 'RADFP_SelectPrevNext');
        theInput.RegisterListener(this, 'OnLogLayerData', 'RADFP_LogDefinition');

        theInput.RegisterListener(this, 'OnInteractiveTime', 'RADFP_AdjustTime');
        theInput.RegisterListener(this, 'OnInteractiveCam', 'RADFP_ToggleInteractiveCam');
    }
    // ------------------------------------------------------------------------
    private function unregisterListeners() {
        parent.unregisterListeners();

        theInput.UnregisterListener(parent, 'RADFP_Quit');

        theInput.UnregisterListener(this, 'RADFP_ToggleUi');

        theInput.UnregisterListener(this, 'RADFP_SetFilter');
        theInput.UnregisterListener(this, 'RADFP_ResetFilter');
        theInput.UnregisterListener(this, 'RADFP_SelectPrevNext');
        theInput.UnregisterListener(this, 'RADFP_LogDefinition');

        theInput.UnregisterListener(this, 'RADFP_AddLayer');
        theInput.UnregisterListener(this, 'RADFP_DelLayer');
        theInput.UnregisterListener(this, 'RADFP_EditLayer');
        theInput.UnregisterListener(this, 'RADFP_CloneLayer');
        theInput.UnregisterListener(this, 'RADFP_UnshadowLayer');
        theInput.UnregisterListener(this, 'RADFP_ToggleVisibility');
        theInput.UnregisterListener(this, 'RADFP_ToggleShadowedLayerVisibility');

        theInput.UnregisterListener(this, 'RADFP_RenameLayer');

        theInput.UnregisterListener(this, 'RADFP_AdjustTime');
        theInput.UnregisterListener(this, 'RADFP_ToggleInteractiveCam');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
