// ----------------------------------------------------------------------------
state RadFp_InteractiveCamera in CRadishFoliagePlanterUi extends Rad_InteractiveCamera
{
    default workContext = 'MOD_RadFp_InteractiveCam';
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        parent.backToPreviousState(action);
    }
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.currentModeId = "InteractiveCamera";
        parent.workContext = this.workContext;

        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractive"));
        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        // interactive cam MUST be stopped before changing to static cam!
        theCam.stopInteractiveMode();

        // reactivate the static cam *AFTER* destroying the interactive one
        parent.switchCamTo(theCam.getActiveSettings());

        super.OnLeaveState(nextStateName);
        parent.notice(GetLocStringByKeyExt("RAD_iCamInteractiveStop"));
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        parent.notice(msg);
    }
    // ------------------------------------------------------------------------
    protected function createCam() : CRadishInteractiveCamera {
        var staticCam: CRadishStaticCamera;
        var interactiveCam: CRadishInteractiveCamera;

        staticCam = parent.getCam();
        interactiveCam = createAndSetupInteractiveCam(
            parent.getConfig(), staticCam.getActiveSettings(), staticCam.getTracker());

        // ahead of view tracking causes visibility glitches for spawned foliage
        interactiveCam.enableAheadOfViewTrackingPosition(false);

        return interactiveCam;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
