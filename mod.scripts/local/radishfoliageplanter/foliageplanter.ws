// ----------------------------------------------------------------------------
class CRadFpUiPopupCallback extends IModUiConfirmPopupCallback {
    public var callback: CRadishFoliagePlanterUi;

    public function OnConfirmed(action: String) {
        switch (action) {
            case "deleteLayer":
                callback.OnDeleteConfirmed();
                break;
            case "cloneLayer":
                callback.OnCloneConfirmed();
                break;
        }
    }
}
// ----------------------------------------------------------------------------
class CRadishPlanterUiListCallback extends IModUiEditableListCallback {
    public var callback: CRadishFoliagePlanterUi;

    public function OnOpened() {
        callback.OnUpdateView();
    }

    public function OnInputEnd(inputString: String) {
        callback.OnInputEnd(inputString);
    }

    public function OnInputCancel() {
        callback.OnInputCancel();
    }

    public function OnClosed() {
        var null: CModUiEditableListView;
        delete this.listMenuRef;
        this.listMenuRef = null;
        callback.OnClosedView();
    }

    public function OnSelected(optionName: String) {
        callback.OnSelected(optionName);
    }
}
// ----------------------------------------------------------------------------
class CRadishPlanterUiPopupCallback extends IModUiPopupMenuCallback {
    public var callback: CRadishFoliagePlanterUi;

    public function OnOpened() {
        callback.OnUpdatePopupView();
    }

    public function OnClosed() {
        var null: CR4Test2Popup;
        delete this.menuRef;
        this.menuRef = null;
    }
}
// ----------------------------------------------------------------------------
state RadFp_Sleeping in CRadishFoliagePlanterUi {}
// ----------------------------------------------------------------------------
statemachine class CRadishFoliagePlanterUi extends CEntity {
    // ------------------------------------------------------------------------
    protected var currentModeId: String;
    protected var workContext: CName;
    // ------------------------------------------------------------------------
    private var autoLogInterval: float; default autoLogInterval = 600.0;
    private var autoLogDefLayer: bool;
    private var isRadFpActive: bool;
    // ------------------------------------------------------------------------
    private var log: CModLogger;
    // ------------------------------------------------------------------------
    // UI stuff
    protected var view: CRadishPlanterUiListCallback;
    protected var viewPopup: CRadishPlanterUiPopupCallback;
    protected var confirmPopup: CModUiActionConfirmation;
    protected var quitConfirmCallback: CRadFpUiQuitPopupCallback;
    protected var popupCallback: CRadFpUiPopupCallback;
    // ------------------------------------------------------------------------
    // flags indicating to pop state/go back to parent *after* currently open
    // view is closed
    private var popStateAfterViewClose: bool;
    // ------------------------------------------------------------------------
    // the real stuff
    protected var layerManager: CRadishFoliageLayerManager;
    protected var foliageManager: CRadishFoliageManager;
    protected var brushManager: CRadishBrushManager;
    protected var foliageEditor: CRadishFoliageEditor;
    // ------------------------------------------------------------------------
    protected var positionTracker: CRadishTerrainTracker;
    protected var terrainProbe: CRadishCachingTerrainProbe;
    // ------------------------------------------------------------------------
    // global cam shared for all managers
    protected var theCam: CRadishStaticCamera;
    // ------------------------------------------------------------------------
    private var configManager: CRadishPlanterConfigManager;
    // ------------------------------------------------------------------------
    public function init(
        log: CModLogger,
        configManager: CRadishPlanterConfigManager,
        quitConfirmCallback: CRadFpUiQuitPopupCallback)
    {
        var stateData: CRadishPlanterStateData;

        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("RADFP_Started"));

        this.log = log;
        this.configManager = configManager;
        this.quitConfirmCallback = quitConfirmCallback;

        // prepare view callback wiring and set labels
        view = new CRadishPlanterUiListCallback in this;
        view.callback = this;

        viewPopup = new CRadishPlanterUiPopupCallback in this;
        viewPopup.callback = this;

        popupCallback = new CRadFpUiPopupCallback in this;
        popupCallback.callback = this;

        this.positionTracker = this.createPositionTracker();
        this.positionTracker.stop();
        this.terrainProbe  = (CRadishCachingTerrainProbe)positionTracker.getTerrainProbe();

        foliageManager = new CRadishFoliageManager in this;
        log.debug("loaded foliage definitions: " + foliageManager.getFoliageList().getTotalCount());

        brushManager = new CRadishBrushManager in this;
        brushManager.init(foliageManager.getFoliageList());
        log.debug("loaded brush definitions: " + brushManager.getBrushList().getTotalCount());

        foliageEditor = createFoliageEditor();
        foliageEditor.init(
            brushManager.getBrushList().getSelectedBrushParams(),
            (CRadishTerrainTracker)positionTracker);

        //stateData = (CRadishPlanterStateData)GetModStorage().load('RadishFoliageUi');

        layerManager = new CRadishFoliageLayerManager in this;
        layerManager.init(this.log, foliageManager.getFoliageList());

        AddTimer('autoLogDefinition', this.autoLogInterval, true, , , , true);

        this.registerListeners();
        GotoState('RadFp_Sleeping');
    }
    // ------------------------------------------------------------------------
    public function activate() : bool {
        this.theCam = this.createStaticCamera();
        this.positionTracker.resume();
        // tracker must be attached to cam because cam position adjusts (invisible)
        // player pos on cam movement and visibility trigger position must be synced
        // with player pos
        theCam.setTracker(positionTracker);
        // ahead of view tracking causes visibility glitches for spawned foliage
        theCam.enableAheadOfViewTrackingPosition(false);
        theCam.activate();
        theCam.setSettings(configManager.getLastCamPosition());
        theCam.switchTo();

        this.isRadFpActive = true;

        this.registerListeners();

        theInput.StoreContext(workContext);

        PushState('RadFp_LayerOverview');

        //TODO restore last edited layer?

        return true;
    }
    // ------------------------------------------------------------------------
    private function createFoliageEditor() : CRadishFoliageEditor {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource(
            "dlc\modtemplates\radishplanterui\base\radish_foliageeditor.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishFoliageEditor)entity;
    }
    // ------------------------------------------------------------------------
    private function createStaticCamera() : CRadishStaticCamera {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource(
            "dlc\modtemplates\radishseeds\static_camera.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishStaticCamera)entity;
    }
    // ------------------------------------------------------------------------
    private function createPositionTracker() : CRadishTerrainTracker {
        var template: CEntityTemplate;
        var entity: CEntity;

        template = (CEntityTemplate)LoadResource(
            "dlc\modtemplates\radishplanterui\base\radish_terraintracker.w2ent", true);
        entity = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CRadishTerrainTracker)entity;
    }
    // ------------------------------------------------------------------------
    public function deactivate(optional isQuitting: bool) {
        this.isRadFpActive = false;

        PopState();
        theInput.RestoreContext(workContext, true);

        this.unregisterListeners();
        this.showUi(false);

        //TODO store last edited layer?

        this.positionTracker.stop();
        // destroy only on quit to prevent cache flushing on minize
        if (isQuitting) {
            this.positionTracker.Destroy();
            this.foliageEditor.destroy();
        }
        theCam.deactivate();
        theCam.Destroy();
        PushState('RadFp_Sleeping');
    }
    // ------------------------------------------------------------------------
    protected function getConfig() : IRadishConfigManager {
        return this.configManager;
    }
    // ------------------------------------------------------------------------
    public function getCam() : CRadishStaticCamera {
        return theCam;
    }
    // ------------------------------------------------------------------------
    public function switchCamTo(placement: SRadishPlacement) {
        theCam.setSettings(placement);
        theCam.switchTo();
    }
    // ------------------------------------------------------------------------
    public function showUi(showUi: bool) {
        if (showUi) {
            if (!view.listMenuRef) {
                theGame.RequestMenu('ListView', view);
            }
        } else {
            if (view.listMenuRef) {
                view.listMenuRef.close();
            }
        }
    }
    // ------------------------------------------------------------------------
    timer function autoLogDefinition(deltaTime: float, id: int) {
        var modeid: CName;

        if (autoLogDefLayer && isRadFpActive) {
            this.log.info("auto-saving definitions to scriptlog...");

            layerManager.logDefinition(true);

            theGame.GetGuiManager().ShowNotification(GetLocStringByKeyExt("RADFP_iAutoDefinitionLogged"));
            autoLogDefLayer = false;
        }
    }
    // ------------------------------------------------------------------------
    public function doQuit() {
        RemoveTimer('autoLogDefinition');

        configManager.setLastCamPosition(theCam.getSettings());
        GetModStorage().save(configManager.getConfig());

        layerManager.logDefinition();
        layerManager.destroy();

        unregisterListeners();
        // to prevent problems with wrongly restored state use this hardcoded
        // "safe" value
        theInput.SetContext('Exploration');

        deactivate(true);
        GetWitcherPlayer().DisplayHudMessage(GetLocStringByKeyExt("RADFP_Stopped"));
    }
    // ------------------------------------------------------------------------
    public function quitRequest() {
        var msgTitle: String;
        var msgText: String;

        //SetIgnoreInput(true);
        if (confirmPopup) { delete confirmPopup; }

        confirmPopup = new CModUiActionConfirmation in this;
        msgTitle = "RADFP_tQuitConfirm";
        msgText = "RADFP_mQuitConfirm";

        confirmPopup.open(quitConfirmCallback,
            GetLocStringByKeyExt(msgTitle),
            GetLocStringByKeyExt(msgText), "quit");
    }
    // ------------------------------------------------------------------------
    event OnQuitRequest(action: SInputAction) {
        if (IsPressed(action)) {
            quitRequest();
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RAD_ToggleWater'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ShowHelp'));
    }
    // ------------------------------------------------------------------------
    protected function getStateNameKey() : String {
        return "RADFP_iHelpMode" + currentModeId;
    }
    // ------------------------------------------------------------------------
    protected function getGeneralHelpKey() : String {
        return "RADFP_mHelp" + currentModeId;
    }
    // ------------------------------------------------------------------------
    event OnHelpMePrettyPlease(action: SInputAction) {
        var helpPopup: CModUiHotkeyHelp;
        var titleKey: String;
        var introText: String;
        var hotkeyList: array<SModUiHotkeyHelp>;

        if (IsPressed(action)) {
            helpPopup = new CModUiHotkeyHelp in this;

            titleKey = "RADFP_tHelpHotkey";
            introText = "<p align=\"left\">" + GetLocStringByKeyExt("RADFP_mHelpCurrentWorkmode")
                + " " + GetLocStringByKeyExt(this.getStateNameKey()) + "</p>";

            introText += "<p>" + GetLocStringByKeyExt(this.getGeneralHelpKey()) + "</p>";

            this.OnHotkeyHelp(hotkeyList);

            helpPopup.open(titleKey, introText, hotkeyList);
        }
    }
    // ------------------------------------------------------------------------
    event OnDeleteConfirmed() {}
    event OnCloneConfirmed() {}
    // ------------------------------------------------------------------------
    event OnUpdatePopupView() {}
    // ------------------------------------------------------------------------
    event OnUpdateView() {}
    event OnInputEnd(inputString: String) {}
    event OnInputCancel() {}
    event OnSelected(listItemId: String) {}
    // ------------------------------------------------------------------------
    event OnClosedView() {
        var null: SInputAction;
        if (popStateAfterViewClose) {
            popStateAfterViewClose = false;
            PopState();
        }
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        theGame.GetGuiManager().ShowNotification(msg);
    }
    // ------------------------------------------------------------------------
    protected function error(errMsg: String) {
        theGame.GetGuiManager().ShowNotification(errMsg);
    }
    // ------------------------------------------------------------------------
    public function isUiShown() : bool {
        return view.listMenuRef;
    }
    // ------------------------------------------------------------------------
    protected function toggleUi() {
        showUi(!isUiShown());
    }
    // ------------------------------------------------------------------------
    protected function backToPreviousState(action: SInputAction) {
        var null: CModUiEditableListView;

        if (IsReleased(action)) {
            if (isUiShown()) {
                // defer restoring previous state to enable clean close of listview
                // otherwise (previous) state tries to open view on OnEnterState but
                // it gets immediately closed again
                popStateAfterViewClose = true;
                // since back is bound to ESCAPE which automatically closes an
                // open menu view the view must no be closed *again*
                delete view.listMenuRef;
                view.listMenuRef = null;
            } else {
                PopState();
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleWater(action: SInputAction) {
        if (IsPressed(action)) {
            positionTracker.toggleWaterVisibilityWithInfo();
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        // -- generic hotkeys
        theInput.RegisterListener(this, 'OnHelpMePrettyPlease', 'RADFP_ShowHelp');
        theInput.RegisterListener(this, 'OnToggleWater', 'RAD_ToggleWater');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        // -- generic hotkeys
        theInput.UnregisterListener(this, 'RADFP_ShowHelp');
        theInput.UnregisterListener(this, 'RAD_ToggleWater');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
