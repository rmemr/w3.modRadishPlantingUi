// ----------------------------------------------------------------------------
class CRadishInteractiveFoliagePlacement extends CRadishInteractivePlacement {
    // ------------------------------------------------------------------------
    private var terrainTracker: CRadishTerrainTracker;
    private var terrainProbe: CRadishCachingTerrainProbe;
    // ------------------------------------------------------------------------
    private var camPos: Vector;
    // ------------------------------------------------------------------------
    public function setTracker(tracker: CRadishTerrainTracker) {
        this.terrainTracker = tracker;
        this.terrainProbe = (CRadishCachingTerrainProbe)tracker.getTerrainProbe();
    }
    // ------------------------------------------------------------------------
    public function setCamPosition(pos: Vector) {
        this.camPos = pos;
    }
    // ------------------------------------------------------------------------
    public function startInteractiveMode(selectedAsset: IRadishPlaceableElement)
    {
        if (!isInteractiveMode) {
            theWorld = theGame.GetWorld();

            asset = selectedAsset;
            placement = asset.getPlacement();
            asset.onPlacementStart();

            refreshConfigSettings();

            // repeats & overrideExisting = true
            AddTimer('updatePlacement', 0.015f, true, , , , true);
            isInteractiveMode = true;
        }
    }
    // ------------------------------------------------------------------------
    public function setAsset(selectedAsset: IRadishPlaceableElement) {
        if (isInteractiveMode) {
            asset = selectedAsset;
            placement = asset.getPlacement();
        }
    }
    // ------------------------------------------------------------------------
    public function stopInteractiveMode() {
        isInteractiveMode = false;
        RemoveTimer('updatePlacement');
        asset.onPlacementEnd();
    }
    // ------------------------------------------------------------------------
    timer function updatePlacement(deltaTime: float, id: int) {
        var newPlacement: SRadishPlacement;
        var directionFB, directionLR: float;
        var moveFB, moveLR, moveUD, moveFBPad, moveLRPad: float;
        var rotateLR, rotateLRPad: float;
        var camDependentStepSize: float;

        var groundPos, groundNormal: Vector;

        newPlacement = placement;

        camDependentStepSize = stepMoveSize * ClampF(VecDistance(camPos, newPlacement.pos), 10.0, 100.0) / 20.0;

        if (((CRadishFoliagePlaceableElement)asset).isRotationMode()) {
            rotateLR = theInput.GetActionValue('GI_MouseDampX');

            newPlacement.rot.Yaw -= (rotateLR + rotateLRPad) * stepRotSize;

        } else {

            moveLR = theInput.GetActionValue('GI_MouseDampX');
            moveFB = - theInput.GetActionValue('GI_MouseDampY');
            moveUD = theInput.GetActionValue('RAD_MoveUpDown');

            // Gamepad Support
            moveLRPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisLeftX');
            moveFBPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisLeftY');
            rotateLRPad = stepGamepadSize * theInput.GetActionValue('RAD_AxisRightX');
            newPlacement.rot.Yaw -= (rotateLR + rotateLRPad) * stepRotSize;

            if (isInteractiveMode && (moveFB != 0 || moveLR != 0 || moveUD != 0 || moveFBPad != 0 || moveLRPad != 0))
            {

                // use current set cam heading so movement is aligned with visuals
                directionFB = Deg2Rad(camHeading + 90);
                directionLR = Deg2Rad(camHeading);

                newPlacement.pos.X += (moveFB + moveFBPad) * camDependentStepSize * CosF(directionFB)
                                    + (moveLR + moveLRPad) * camDependentStepSize * CosF(directionLR);

                newPlacement.pos.Y += (moveFB + moveFBPad) * camDependentStepSize * SinF(directionFB)
                                    + (moveLR + moveLRPad) * camDependentStepSize * SinF(directionLR);

                // somehow this is much slower than with mouse adjustments
                newPlacement.pos.Z += moveUD * camDependentStepSize * 4;

                // deactivate snaptoGround when up/down hotkeys are used
                if (moveUD != 0) {
                    snapToGround = false;
                }

                // adjust to world surface
                if (snapToGround && terrainProbe.getApproximatedGroundPos(newPlacement.pos, groundPos)) {
                    //TODO signal failed ground detection?
                    newPlacement.pos.Z = groundPos.Z;
                }
            }
        }

        if (newPlacement != placement) {
            asset.setPlacement(newPlacement);
            placement = newPlacement;
        }
    }
    // ------------------------------------------------------------------------
    /*
    public function snapToGround() {
        var groundZ: float;

        // adjust to world surface
        if (theWorld.PhysicsCorrectZ(placement.pos, groundZ)) {
            placement.pos.Z = groundZ;
            asset.setPlacement(placement);
        }
    }*/
    // ------------------------------------------------------------------------
    public function refreshConfigSettings(optional mode: String) {
        var conf: SRadishPlacementConfig;

        conf = configProvider.getPlacementConfig();
        switch (mode) {
            case "slow":
                stepMoveSize = conf.slow.stepMove;
                stepRotSize = conf.slow.stepRot;
                break;

            case "fast":
                stepMoveSize = conf.fast.stepMove;
                stepRotSize = conf.fast.stepRot;
                break;

            default:
                stepMoveSize = conf.normal.stepMove;
                stepRotSize = conf.normal.stepRot;
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
