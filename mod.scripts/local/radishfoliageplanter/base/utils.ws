// ----------------------------------------------------------------------------
function RadFpUi_escapeAsId(input: String) : String {
    var i, s: int;
    var result, char: String;

    s = StrLen(input);
    for (i = 0; i < s; i += 1) {
        char = StrMid(input, i, 1);
        if (StrFindFirst("abcdefghijklmnopqrstuvwxyz_1234567890~", char) >= 0) {
            result += char;
        } else {
            result += "_";
        }
    }
    return result;
}
// ----------------------------------------------------------------------------
