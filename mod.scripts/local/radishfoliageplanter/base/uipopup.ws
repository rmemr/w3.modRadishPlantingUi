// ----------------------------------------------------------------------------
// "UIView" -> "Controller" callback "interfaces"
abstract class IModUiPopupMenuCallback extends IModUiMenuCallback {
    public var menuRef: CR4Test2Popup;

    public function OnConfigUi() {
        // defaults
        menuRef.setMenuConfig(SModUiTopMenuConfig(
            40.0, 35.0, 60.0, 25.0, 25.0,   // x, y, width, height, alpha
            SModUiTextFieldConfig(          // title field:
                1, 37, 130, 25, 18              // x, y, width, height, fontsize
            ),
            SModUiTextFieldConfig(          // title field:
                3, 82, 230, 25, 14              // x, y, width, height, fontsize
            ),
            SModUiTextFieldConfig(          // title field:
                3, 125, 330, 25, 12             // x, y, width, height, fontsize
            ),
            SModUiTextFieldConfig(          // title field:
                3, 160, 330, 25, 8              // x, y, width, height, fontsize
            ),
        ));
    }
}
// ----------------------------------------------------------------------------
class CR4Test2Popup extends CR4Popup {
    protected var callback: IModUiPopupMenuCallback;

    // -- some configurable options
    private var menuConfig: SModUiTopMenuConfig;

    // text field ids
    private var textFieldId1: String;
    private var textFieldId2: String;
    private var textFieldId3: String;
    private var textFieldId4: String;
    // ------------------------------------------------------------------------
    event OnConfigUI() {
        callback = (IModUiPopupMenuCallback)GetPopupInitData();
        callback.menuRef = this;

        // -- setup configs for pos, size, etc for additional title, stats and edit fields
        callback.OnConfigUi();

        // ids of usable fields
        textFieldId1 = "tfText";
        textFieldId2 = "tfText2";
        textFieldId3 = "tfText3";
        textFieldId4 = "tfText4";

        setupFields();

        callback.OnOpened();
    }
    // ------------------------------------------------------------------------
    public function setMenuConfig(conf: SModUiTopMenuConfig) {
        menuConfig = conf;
    }
    // ------------------------------------------------------------------------
    public function getMenuConfig() : SModUiTopMenuConfig {
        return menuConfig;
    }
    // ------------------------------------------------------------------------
    public function setupFields() {
        var popup, background, textField : CScriptedFlashSprite;

        popup = GetPopupFlash().GetChildFlashSprite("Empty");

        background = popup.GetChildFlashSprite("mcBackground");
        background.SetX(menuConfig.x);
        background.SetY(menuConfig.y);
        background.SetXScale(menuConfig.width);
        background.SetYScale(menuConfig.height);
        background.SetAlpha(menuConfig.alpha);

        textField = popup.GetChildFlashSprite(textFieldId1);
        textField.SetX(menuConfig.titleField.x);
        textField.SetY(menuConfig.titleField.y);
        //textField.SetXScale(menuConfig.titleField.width);
        //textField.SetYScale(menuConfig.titleField.height);

        textField = popup.GetChildFlashSprite(textFieldId2);
        textField.SetX(menuConfig.statsField.x);
        textField.SetY(menuConfig.statsField.y);
        //textField.SetXScale(menuConfig.statsField.width);
        //textField.SetYScale(menuConfig.statsField.height);

        textField = popup.GetChildFlashSprite(textFieldId3);
        textField.SetX(menuConfig.editField.x);
        textField.SetY(menuConfig.editField.y);
        //textField.SetXScale(menuConfig.editField.width);
        //textField.SetYScale(menuConfig.editField.height);

        textField = popup.GetChildFlashSprite(textFieldId4);
        textField.SetX(menuConfig.extraField.x);
        textField.SetY(menuConfig.extraField.y);
        //textField.SetXScale(menuConfig.extraField.width);
        //textField.SetYScale(menuConfig.extraField.height);

        // -- clear fields
        setTextField1("");
        setTextField2("");
        setTextField3("");
        setTextField4("");
    }
    // ------------------------------------------------------------------------
    public function setTextField1(newText: String, optional color: String) {
        setText(textFieldId1, newText, menuConfig.titleField.fontSize, color);
    }
    // ------------------------------------------------------------------------
    public function setTextField2(newText: String, optional color: String) {
        setText(textFieldId2, newText, menuConfig.statsField.fontSize, color);
    }
    // ------------------------------------------------------------------------
    public function setTextField3(newText: String, optional color: String) {
        setText(textFieldId3, newText, menuConfig.editField.fontSize, color);
    }
    // ------------------------------------------------------------------------
    public function setTextField4(newText: String, optional color: String) {
        setText(textFieldId4, newText, menuConfig.extraField.fontSize, color);
    }
    // ------------------------------------------------------------------------
    protected function setText(
        fieldId: String, newText: String, fontSize: int, optional color: String)
    {
        var fontCol: String;
        var popup : CScriptedFlashSprite;
        var textField : CScriptedFlashTextField;

        if (color == "") { fontCol = "color=\"#e5e5e5\">"; } else { fontCol = "color=\"" + color + "\">"; }

        popup = GetPopupFlash().GetChildFlashSprite("Empty");
        textField = popup.GetChildFlashTextField(fieldId);
        textField.SetTextHtml("<font size=\"" + IntToString(fontSize) + "\" " + fontCol + newText + "<font>");
    }
    // ------------------------------------------------------------------------
    event OnClosingPopup() { }
    event OnClosePopup() {
        ClosePopup();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
