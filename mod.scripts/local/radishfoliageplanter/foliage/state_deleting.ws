// ----------------------------------------------------------------------------
class CRadishDeleteCmd extends CRadishPlantingCmd {
    // ------------------------------------------------------------------------
    public function execute() : CRadishFoliageSet {
        var i: int;

        for (i = 0; i < foliageSet.type.Size(); i += 1) {
            // set to -1 to delete
            foliageSet.type[i] = -1;
        }

        return foliageSet;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFp_Deleting in CRadishFoliageEditor extends RadFp_PlantingMode {
    // ------------------------------------------------------------------------
    default currentMode = 'RADFP_DeletingMode';
    default currentModeColor = "red";
    default maxBrushRadius = 100.0;
    // ------------------------------------------------------------------------
    protected var cmd: CRadishDeleteCmd;
    // ------------------------------------------------------------------------
    event OnCurrentParamInfo(out info: String) {
        if (filterByType) {
            info = "";
        } else {
            info = "[" + GetLocStringByKeyExt("RADFP_lNoActiveFilter") + "]";
        }
    }
    // ------------------------------------------------------------------------
    protected function onCmd1Start() {
        cmd = new CRadishDeleteCmd in this;

        super.onCmd1Start();

        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(), brush.getRadius(), getFilteredTypes());

        cmd.init(
            brush.getCenterPosition(), brush.getRadius(), parent.currentBrushParams,
            foliageSet);

        foliageSet = cmd.execute();

        parent.editedLayer.updateSet(foliageSet);

        // command is synchronous
        onCmd1End();
    }
    // ------------------------------------------------------------------------
    protected function onCmd2Start() {
        // same as cmd 1 (since thinning also removes on cmd2 -> consistency)
        onCmd1Start();
    }
    // ------------------------------------------------------------------------
    // deactivate these commands
    protected function onCmd3Start() {}
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op1', "RADFP_DeleteOp"));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op2', "RADFP_DeleteOp"));
        super.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
