// ----------------------------------------------------------------------------
class CRadishScalingCmd extends CRadishPlantingCmd {
    // ------------------------------------------------------------------------
    private var scalePercentage: float;
    // ------------------------------------------------------------------------
    public function setParams(scalePercentage: float) {
        this.scalePercentage = scalePercentage;
    }
    // ------------------------------------------------------------------------
    public function execute() : CRadishFoliageSet {
        var i: int;
        for (i = 0; i < foliageSet.type.Size(); i += 1) {
            // range accepted by encoder [0.2, 3.0]
            foliageSet.scale[i] = ClampF(foliageSet.scale[i] * scalePercentage, 0.2, 3.0);
        }
        return foliageSet;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFp_Scaling in CRadishFoliageEditor extends RadFp_PlantingMode {
    // ------------------------------------------------------------------------
    default currentMode = 'RADFP_ScalingMode';
    default currentModeColor = "yellow";
    default maxBrushRadius = 100.0;
    // ------------------------------------------------------------------------
    protected var scalingModifierLow: int;    default scalingModifierLow = 2;
    protected var scalingModifierNormal: int; default scalingModifierNormal = 5;
    protected var scalingModifierHigh: int;   default scalingModifierHigh = 15;
    // ------------------------------------------------------------------------
    private var editedFoliage: array<CRadishFoliageEntity>;
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        parent.RemoveTimers();
        hideFoliageWireframes(1.0, 0);
        super.OnLeaveState(nextStateName);
    }
    // ------------------------------------------------------------------------
    protected function getScalingModifier() : int {
        switch (parent.intensity) {
            case 1:     return scalingModifierLow;
            case 2:     return scalingModifierNormal;
            default:    return scalingModifierHigh;
        }
    }
    // ------------------------------------------------------------------------
    protected function formatCurrentParamInfo(optional additional: String) : String
    {
        var info, filterInfo: String;

        // [scaling: 5%]
        if (filterByType) {
            filterInfo = "";
        } else {
            filterInfo = GetLocStringByKeyExt("RADFP_lNoActiveFilter") + " ";
        }

        info = "[" + filterInfo
            + GetLocStringByKeyExt("RADFP_lScaling") + " " + IntToString(getScalingModifier())
            + "%";

        if (additional != "") { info += ", " + additional; }
        return info + "]";
    }
    // ------------------------------------------------------------------------
    event OnCurrentParamInfo(out info: String) {
        info = formatCurrentParamInfo();
    }
    // ------------------------------------------------------------------------
    protected function onCmd1Start() {
        var cmd: CRadishScalingCmd;

        cmd = new CRadishScalingCmd in this;
        super.onCmd1Start();

        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(), brush.getRadius(), getFilteredTypes());

        cmd.init(
            brush.getCenterPosition(), brush.getRadius(), parent.currentBrushParams,
            foliageSet);
        cmd.setParams(1.0 + getScalingModifier() / 100.0);

        showFoliageWireframes(foliageSet);

        foliageSet = cmd.execute();
        parent.editedLayer.updateSet(foliageSet);

        // command is synchronous
        onCmd1End();
    }
    // ------------------------------------------------------------------------
    protected function onCmd2Start() {
        var cmd: CRadishScalingCmd;

        cmd = new CRadishScalingCmd in this;
        super.onCmd2Start();

        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(), brush.getRadius(), getFilteredTypes());

        cmd.init(
            brush.getCenterPosition(), brush.getRadius(), parent.currentBrushParams,
            foliageSet);
        cmd.setParams(1.0 - getScalingModifier() / 100.0);

        showFoliageWireframes(foliageSet);

        foliageSet = cmd.execute();
        parent.editedLayer.updateSet(foliageSet);

        // command is synchronous
        onCmd2End();
    }
    // ------------------------------------------------------------------------
    protected function showFoliageWireframes(foliageSet: CRadishFoliageSet) {
        var i: int;

        for (i = 0; i < foliageSet.proxies.Size(); i += 1) {
            if (!editedFoliage.Contains(foliageSet.proxies[i])) {
                foliageSet.proxies[i].showWireframe();
                editedFoliage.PushBack(foliageSet.proxies[i]);
            }
        }
        parent.RemoveTimer('hideFoliageWireframes');
        parent.AddTimer('hideFoliageWireframes', 1.0, false);
    }
    // ------------------------------------------------------------------------
    timer function hideFoliageWireframes(delta : float, id : int) {
        var i: int;

        for (i = 0; i < editedFoliage.Size(); i += 1) {
            editedFoliage[i].hideWireframe();
        }
        editedFoliage.Clear();
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op1', "RADFP_ScalingUpOp"));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op2', "RADFP_ScalingDownOp"));
        super.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
    // deactivate these commands
    protected function onCmd3Start() {}
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
