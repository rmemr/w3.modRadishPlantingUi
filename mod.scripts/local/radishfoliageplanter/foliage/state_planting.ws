// ----------------------------------------------------------------------------
class CRadishPlantCmd extends CRadishDistanceAwarePlantCmd {
    // ------------------------------------------------------------------------
    private function plantSingleFoliageAtCenter() : CRadishFoliageSet {
        var selectionRange, foliageSlot, foliageType: int;
        var pos: Vector;
        var scale, rot: float;

        // note: no collision detection
        selectionRange = brushParams.typeSelection.Size();

        pos = brushCenter;
        foliageSlot = brushParams.typeSelection[RandRange(selectionRange, 0)];
        foliageType = brushParams.foliage[foliageSlot].typeSlot;

        // add new foliage
        scale = RandRangeF(
            brushParams.foliage[foliageSlot].scaleRange.max,
            brushParams.foliage[foliageSlot].scaleRange.min
        );
        rot = RandRangeF(
            brushParams.foliage[foliageSlot].rotRange.max,
            brushParams.foliage[foliageSlot].rotRange.min
        );
        foliageSet.type.PushBack(foliageType);
        foliageSet.pos.PushBack(pos);
        foliageSet.rot.PushBack(rot);
        foliageSet.scale.PushBack(scale);
        foliageSet.slotInLayer.PushBack(-1);

        return foliageSet;
    }
    // ------------------------------------------------------------------------
    private function plantFoliageInBrushArea() : CRadishFoliageSet {
        var selectionRange, foliageSlot, foliageType, x, y: int;
        var gridTileWidth, randPosRange: float;
        var gridTiles: int;
        var pos: Vector;
        var scale, rot, baseX, baseY: float;

        brushCenter.Z = 0.0;
        brushCenter.W = 1.0;

        selectionRange = brushParams.typeSelection.Size();
        randPosRange = ClampF(densityDistance, 0.1, 100);

        // max 2000 ~ 0.1 steps for brushRadius 100
        gridTiles = CeilF(ClampF(2.0 * brushRadius / randPosRange, 1, 2000.0));

        // this should be ~ densityDistance but after clamping!
        gridTileWidth = 2.0 * brushRadius / gridTiles;

        // grid is rectangular but pos will be radius filtered in the loop (*)
        baseX = brushCenter.X - brushRadius - 0.5 * randPosRange;
        baseY = brushCenter.Y - brushRadius - 0.5 * randPosRange;

        for (y = 0; y < gridTiles; y += 1) {
            baseX = brushCenter.X - brushRadius - 0.5 * randPosRange;
            baseY += gridTileWidth;

            for (x = 0; x < gridTiles; x += 1) {
                baseX += gridTileWidth;
                pos = Vector(
                    baseX + RandRangeF(randPosRange, -randPosRange),
                    baseY + RandRangeF(randPosRange, -randPosRange),
                    // marks an unset ground position (important to skip already
                    // set groundpos for extracted foliage in the foliageset)
                    -100000.0
                );

                // (*) don't plant outside brush area
                if (VecLength2D(brushCenter - pos) > brushRadius) {
                    continue;
                }
                // extra parameter to allow a little more control over density
                // with (intensity/probability) hotkeys
                if (RandF() >= probability) { continue; }

                foliageSlot = brushParams.typeSelection[RandRange(selectionRange, 0)];
                foliageType = brushParams.foliage[foliageSlot].typeSlot;

                // check collision
                if (!checkMinimumDistance(pos, foliageType, brushParams.foliage[foliageSlot].minDistance)) {
                    continue;
                }

                // add new foliage
                scale = RandRangeF(
                    brushParams.foliage[foliageSlot].scaleRange.max,
                    brushParams.foliage[foliageSlot].scaleRange.min
                );
                rot = RandRangeF(
                    brushParams.foliage[foliageSlot].rotRange.max,
                    brushParams.foliage[foliageSlot].rotRange.min
                );
                foliageSet.type.PushBack(foliageType);
                foliageSet.pos.PushBack(pos);
                foliageSet.rot.PushBack(rot);
                foliageSet.scale.PushBack(scale);
                foliageSet.slotInLayer.PushBack(-1);

                // add new foliage into brush set to check minimum distances in
                // subsequent planting
                addToMinDistanceSet(foliageSet.type.Size() - 1);
            }
        }
        return foliageSet;
    }
    // ------------------------------------------------------------------------
    public function execute() : CRadishFoliageSet {
        // special case very small brush
        if (brushRadius <= 0.1) {
            return plantSingleFoliageAtCenter();
        } else {
            // otherwise randomized position + planting (potentially no planting at all)
            return plantFoliageInBrushArea();
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishThinningCmd extends CRadishPlantingCmd {
    // ------------------------------------------------------------------------
    private var probability: float;
    // ------------------------------------------------------------------------
    public function setParams(probability: float) {
        this.probability = probability;
    }
    // ------------------------------------------------------------------------
    public function execute() : CRadishFoliageSet {
        var i: int;
        for (i = 0; i < foliageSet.type.Size(); i += 1) {
            if (RandF() < probability) {
                // set to -1 to delete
                foliageSet.type[i] = -1;
            }
        }
        return foliageSet;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFp_Planting in CRadishFoliageEditor extends RadFp_PlantingMode {
    // ------------------------------------------------------------------------
    default currentMode = 'RADFP_PlantingMode';
    default currentModeColor = "green";
    default maxBrushRadius = 100.0;
    // ------------------------------------------------------------------------
    protected var probabilityLow: int;          default probabilityLow = 40;
    protected var probabilityNormal: int;       default probabilityNormal = 65;
    protected var probabilityHigh: int;         default probabilityHigh = 85;
    // ------------------------------------------------------------------------
    protected var deleteProbModifier: float;    default deleteProbModifier = 0.0065;
    // ------------------------------------------------------------------------
    protected var densityModifierLow: float;    default densityModifierLow = 1.35;
    protected var densityModifierNormal: float; default densityModifierNormal = 1.0;
    protected var densityModifierHigh: float;   default densityModifierHigh = 0.65;
    // ------------------------------------------------------------------------
    protected var cmd: CRadishPlantingCmd;
    protected var lastTrackedPos: SRadishPlacement;
    // ------------------------------------------------------------------------
    // intensity params
    // ------------------------------------------------------------------------
    protected function getProbability() : int {
        switch (parent.intensity) {
            case 1:     return probabilityLow;
            case 2:     return probabilityNormal;
            default:    return probabilityHigh;
        }
    }
    // ------------------------------------------------------------------------
    protected function getDensityModifier() : float {
        switch (parent.intensity) {
            case 1:     return densityModifierLow;
            case 2:     return densityModifierNormal;
            default:    return densityModifierHigh;
        }
    }
    // ------------------------------------------------------------------------
    protected function formatCurrentParamInfo(optional additional: String) : String
    {
        var density: float;
        var info, filterInfo: String;
        // [probability: 40%, density: 4.0]
        // [probability: 40%, density: 4.0, planting: 100/1344]

        density = parent.currentBrushParams.densityDistance * getDensityModifier();

        if (filterByType) {
            filterInfo = "";
        } else {
            filterInfo = GetLocStringByKeyExt("RADFP_lNoActiveFilter") + " ";
        }

        info = "[" + filterInfo
            + GetLocStringByKeyExt("RADFP_lProbablity") + " " + IntToString(getProbability())
            + "%, " + GetLocStringByKeyExt("RADFP_lDensity") + " ";

        if (density < 2.0) {
            info += FloatToStringPrec(density, 2);
        } else {
            info += FloatToStringPrec(density, 1);
        }
        if (additional != "") { info += ", " + additional;}
        return info + "]";
    }
    // ------------------------------------------------------------------------
    event OnCurrentParamInfo(out info: String) {
        info = formatCurrentParamInfo();
    }
    // ------------------------------------------------------------------------
    // adding of foliage in brush
    // ------------------------------------------------------------------------
    protected function onCmd1Start() {
        super.onCmd1Start();
        // delegate to entry type method to allow async processing (with sleep)
        // because big brush areas tend to freeze for a while
        // with asdync processing the state updates (e.g. generating plants)
        // can be displayed for users
        asyncCmd1Start();
    }
    // ------------------------------------------------------------------------
    private entry function asyncCmd1Start() {
        cmd = new CRadishPlantCmd in this;

        lastTrackedPos = parent.tracker.getTrackedPos();
        parent.tracker.updateTrackedPos(SRadishPlacement(brush.getCenterPosition()));

        // increase radius to allow for minimumrange checks outside of brush
        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(),
            brush.getRadius() + parent.currentBrushParams.maxDistance);

        cmd.init(
            brush.getCenterPosition(), brush.getRadius(), parent.currentBrushParams,
            foliageSet);
        ((CRadishPlantCmd)cmd).setParams(
            getProbability(),
            parent.currentBrushParams.densityDistance * getDensityModifier());

        // since generating foliage for big brush areas can take a while set
        // view status update and...
        parent.view.setOperationInProgress(
            formatCurrentParamInfo(
                GetLocStringByKeyExt("RADFP_lPlantingFoliageGenerating")));
        // ...sleep (to wait for rendering state)
        Sleep(0.1);

        foliageSet = cmd.execute();

        if (brush.isMinimumSize()) {
            parent.notice(GetLocStringByKeyExt("RADFP_iBrushCenterPlanting"));
        }
        // this will definitely take a while for bigger brushes
        // -> dedicated progress updates in view
        asyncUpdateTerrainPositions();
    }
    // ------------------------------------------------------------------------
    private entry function asyncUpdateTerrainPositions() {
        var counter, currentSlot, i, s, retries, failures: int;
        var p, groundPos: Vector;
        var posOrderedSlots: array<SRadSortKeyValue>;

        s = foliageSet.pos.Size();

        // reorder generated foliage based on position so the trailing position
        // tracker doesn't need to cross as much ground between subsequent
        // foliage entries in the sequence (-> much faster)
        for (i = 0; i < s; i += 1) {
            p = foliageSet.pos[i];
            posOrderedSlots.PushBack(
                // quantize to 10m stripes
                SRadSortKeyValue(FloorF(p.X / 10.0), RoundF(p.Y), i)
            );
        }
        radMergeSortKeyValue(posOrderedSlots);

        // iterate in position-ordered sequence
        counter = 0;
        while (counter < s) {
            currentSlot = posOrderedSlots[counter].payload;

            // skip foliage that already has ground position (== foliage existing
            // in brush area upon cmd start)
            if (foliageSet.pos[currentSlot].Z > -100000.0) {
                counter += 1;
                continue;
            }

            if (parent.terrainProbe.getExactGroundPos(foliageSet.pos[currentSlot], groundPos)) {
                retries = 0;
                foliageSet.pos[currentSlot] = groundPos;
                // preceed to next position
                counter += 1;
            } else {
                if (retries >= 10) {
                    LogChannel('DEBUG', "Planting command: failed to get exact terrain height for plant! removing...");
                    foliageSet.type[currentSlot] = -1;
                    failures += 1;
                    counter += 1;
                } else {
                    //LogChannel('DEBUG', "Planting command: updating terrainprobe position to ["
                    //    + VecToString(foliageSet.pos[counter]) + "]");
                    parent.tracker.updateTrackedPos(SRadishPlacement(foliageSet.pos[currentSlot]));
                    retries += 1;

                    parent.view.setOperationInProgress(
                        formatCurrentParamInfo(
                            GetLocStringByKeyExt("RADFP_lPlanting")
                            + " <font color=\"#ff5555\">"
                            + IntToString(counter) + "/" + IntToString(s)
                            + "</font>"
                    ));

                    Sleep(0.1);
                }
            }
        }
        if (failures > 0) {
            parent.error(GetLocStringByKeyExt("RADFP_eFailedPlantingCount") + failures);
        }
        onCmd1End();
    }
    // ------------------------------------------------------------------------
    protected function onCmd1End() {
        // return to cam position to prevent foliage visibility glitches
        parent.tracker.updateTrackedPos(lastTrackedPos);
        parent.editedLayer.updateSet(foliageSet);
        super.onCmd1End();
    }
    // ------------------------------------------------------------------------
    // thinning of foliage in brush
    // ------------------------------------------------------------------------
    protected function onCmd2Start() {
        cmd = new CRadishThinningCmd in this;
        super.onCmd1Start();

        foliageSet = parent.editedLayer.getInRange(
            brush.getCenterPosition(), brush.getRadius(), getFilteredTypes());

        cmd.init(
            brush.getCenterPosition(), brush.getRadius(), parent.currentBrushParams,
            foliageSet);
        ((CRadishThinningCmd)cmd).setParams(getProbability() * deleteProbModifier);

        foliageSet = cmd.execute();
        parent.editedLayer.updateSet(foliageSet);

        // command is synchronous
        onCmd2End();
    }
    // ------------------------------------------------------------------------
    // disable cmd 3
    protected function onCmd3Start() {}
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op1', "RADFP_PlantingOp"));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_Op2', "RADFP_ThinningOp"));
        super.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// based on https://www.techiedelight.com/iterative-merge-sort-algorithm-bottom-up/
// ----------------------------------------------------------------------------
struct SRadSortKeyValue {
    var ord1: int;
    var ord2: int;
    var payload: int;
}
// ----------------------------------------------------------------------------
function radMergeSortKeyValueMerge(
    out elements: array<SRadSortKeyValue>, out temp: array<SRadSortKeyValue>,
    from: int, mid: int, to: int)
{
    var k, i, j: int;
    var idA, idB: String;
    var a, b: SRadSortKeyValue;

    var isSmaller: bool;

    k = from;
    i = from;
    j = mid + 1;

    while (i <= mid && j <= to) {
        a = elements[i];
        b = elements[j];

        if ( (a.ord1 < b.ord1) || (a.ord1 == b.ord1 && a.ord2 <= b.ord2)) {
            temp[k] = elements[i];
            i += 1;
        } else {
            temp[k] = elements[j];
            j += 1;
        }
        k += 1;
    }

    while (i <= mid) {
        temp[k] = elements[i];
        k += 1;
        i += 1;
    }

    for (i = from; i <= to; i += 1) {
        elements[i] = temp[i];
    }
}
// ----------------------------------------------------------------------------
function radMergeSortKeyValue(out elements: array<SRadSortKeyValue>) {
    var low, high: int;
    var m, i: int;
    var from, mid, to: int;
    var temp: array<SRadSortKeyValue>;

    low = 0;
    high = elements.Size() - 1;

    for (i = 0; i <= high; i += 1) {
        temp.PushBack(elements[i]);
    }

    for (m = 1; m <= high - low; m = 2 * m) {
        for (i = low; i < high; i += 2 * m) {
            from = i;
            mid = i + m - 1;
            to = Min(i + 2 * m - 1, high);

            radMergeSortKeyValueMerge(elements, temp, from, mid, to);
        }
    }
}
// ----------------------------------------------------------------------------
