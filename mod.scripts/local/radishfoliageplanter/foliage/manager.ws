// ----------------------------------------------------------------------------
struct SRadFoliageProxyInfo {
    var mips: array<int>;
    var size: Vector;
    var min: Vector;
}
// ----------------------------------------------------------------------------
class CRadishFoliageList extends CRadishFpUiFilteredList {
    // ------------------------------------------------------------------------
    protected var templates: array<CEntityTemplate>;
    protected var minDistances: array<SRangeF>;
    protected var meta: array<SRadFoliageProxyInfo>;
    // ------------------------------------------------------------------------
    public function loadCsv(foliageListCsv: String, foliageMetaCsv: String) {
        var nullTemplate: CEntityTemplate;
        var data: C2dArray;
        var metaCsv: C2dArray;
        var metaInfo: array<SRadFoliageProxyInfo>;
        var i, m, mipLevel, metaSlot: int;
        var idLookup: array<String>;
        var mips: array<int>;
        var id: String;
        var dim: float;

        // load and parse all the meta data. assign to correct slot later
        metaCsv = LoadCSV(foliageMetaCsv);
        // csv: ;ID;size1;size2;size3;min.X;min.Y;min.Z;mip0;mip1;mip2;mip3;mip4;mip5;mip6;mip7;mip8;mip9
        for (i = 0; i < metaCsv.GetNumRows(); i += 1) {
            idLookup.PushBack(metaCsv.GetValueAt(1, i));

            mips.Clear();
            for (m = 8; m <= 17; m += 1) {
                mipLevel = StringToInt(metaCsv.GetValueAt(m, i), -1);
                if (mipLevel > 0) {
                    mips.PushBack(mipLevel);
                } else {
                    break;
                }
            }
            metaInfo.PushBack(SRadFoliageProxyInfo(
                mips,
                // it seems the coordinate system used in srt has different orientation
                // X <-> Y
                Vector(
                    StringToFloat(metaCsv.GetValueAt(3, i), -1.0),
                    StringToFloat(metaCsv.GetValueAt(2, i), -1.0),
                    StringToFloat(metaCsv.GetValueAt(4, i), -1.0)
                ),
                Vector(
                    StringToFloat(metaCsv.GetValueAt(6, i), -1.0),
                    StringToFloat(metaCsv.GetValueAt(5, i), -1.0),
                    StringToFloat(metaCsv.GetValueAt(7, i), -1.0)
                )
            ));
        }

        data = LoadCSV(foliageListCsv);
        // csv: col0;CAT1;CAT2;CAT3;id;caption;template
        for (i = 0; i < data.GetNumRows(); i += 1) {
            id = data.GetValueAt(4, i);
            items.PushBack(SModUiCategorizedListItem(
                id,
                data.GetValueAt(5, i),
                data.GetValueAt(1, i),
                data.GetValueAt(2, i),
                data.GetValueAt(3, i)
            ));

            // find slot with parsed metadata by foliage id
            metaSlot = idLookup.FindFirst(id);
            meta.PushBack(metaInfo[metaSlot]);

            // calculate min distance based foliage dimensions
            dim = MaxF(metaInfo[metaSlot].size.X, metaInfo[metaSlot].size.Y);

            minDistances.PushBack(SRangeF(
                // distance to same foliage type, allow minor overlapping as this
                // is only a hard constraint and actual distance will be rtandomized
                ClampF(dim * 0.9, 0.2, 100),
                // distance to other types should be almost always much lower
                ClampF(dim * 0.5, 0.2, 1),
            ));

            templates.PushBack(nullTemplate);
        }
    }
    // ------------------------------------------------------------------------
    public function getTemplate(slotId: int) : CEntityTemplate {
        var null, template: CEntityTemplate;
        var foliageId: String;

        foliageId = items[slotId].id;

        template = templates[slotId];
        if (template == null) {
            template = (CEntityTemplate)LoadResource(
                "dlc/modtemplates/radishplanterui/foliage/" + foliageId + "_100.w2ent", true);
            templates[slotId] = template;
        }
        return template;
    }
    // ------------------------------------------------------------------------
    public function getMetaInfo(slotId: int) : SRadFoliageProxyInfo {
        return meta[slotId];
    }
    // ------------------------------------------------------------------------
    public function getSlotId(strId: String) : int {
        var i, s: int;

        for (i = 0; i < items.Size(); i += 1) {
            if (items[i].id == strId) {
                return i;
            }
        }
        return -1;
    }
    // ------------------------------------------------------------------------
    public function getStringId(slotId: int) : String {
        return items[slotId].id;
    }
    // ------------------------------------------------------------------------
    public function getMinDistances(slotId: int) : SRangeF {
        return minDistances[slotId];
    }
    // ------------------------------------------------------------------------
    public function getItemList() : array<SModUiCategorizedListItem> {
        return items;
    }
    // ------------------------------------------------------------------------
    public function clear() {
        items.Clear();
        templates.Clear();
        meta.Clear();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CRadishFoliageManager {
    private var foliageList: CRadishFoliageList;
    private var dataLoaded: bool;
    // ------------------------------------------------------------------------
    private function lazyLoad() {
        var additional: array<SRadFoliage_CustomSrtCsvDef>;
        var i: int;

        foliageList = new CRadishFoliageList in this;

        foliageList.clear();
        //foliageList.addExtraFoliage(RADFP_getExtraFoliage());

        additional = RADFP_getAdditionalSrtDefCsvs();
        for (i = 0; i < additional.Size(); i += 1) {
            foliageList.loadCsv(additional[i].foliageRepo, additional[i].foliageMeta);
        }

        foliageList.loadCsv(
            "dlc\dlcradishplantingui\data\all-foliage-repo.csv",
            "dlc\dlcradishplantingui\data\all-foliage-meta.csv");

        dataLoaded = true;
    }
    // ------------------------------------------------------------------------
    public function getFoliageList() : CRadishFoliageList {
        if (!dataLoaded) { lazyLoad(); }

        return foliageList;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
