// ----------------------------------------------------------------------------
abstract state RadFp_PlantingMode in CRadishFoliageEditor {
    // ------------------------------------------------------------------------
    protected var currentMode: CName;
    protected var currentModeColor: String;
    protected var maxBrushRadius: Float;
    protected var foliageSet: CRadishFoliageSet;
    protected var brush: CRadishFoliageBrush;
    protected var filterByType: bool; default filterByType = true;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.currentMode = this.currentMode;
        this.brush = parent.brush;
        this.brush.setSizeConstraint(maxBrushRadius);
        this.brush.setColor(currentModeColor);
        parent.view.refresh();
        registerListeners();
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        unregisterListeners();
    }
    // ------------------------------------------------------------------------
    protected function onCmd1Start() {
        parent.cmdActive = true;
        brush.onStartOperation();
    }
    protected function onCmd2Start() {
        parent.cmdActive = true;
        brush.onStartOperation();
    }
    protected function onCmd3Start() {
        parent.cmdActive = true;
        brush.onStartOperation();
    }
    // ------------------------------------------------------------------------
    protected function onCmd1End() {
        brush.onEndOperation();
        parent.cmdActive = false;
        parent.view.setOperationInProgress("");
    }
    protected function onCmd2End() {
        brush.onEndOperation();
        parent.cmdActive = false;
        parent.view.setOperationInProgress("");
    }
    protected function onCmd3End() {
        brush.onEndOperation();
        parent.cmdActive = false;
        parent.view.setOperationInProgress("");
    }
    // ------------------------------------------------------------------------
    event OnOperation(action: SInputAction) {
        if (!parent.cmdActive && !parent.locked && IsPressed(action)) {
            switch (action.aName) {
                case 'RADFP_Op1': this.onCmd1Start(); break;
                case 'RADFP_Op2': this.onCmd2Start(); break;
                case 'RADFP_Op3': this.onCmd3Start(); break;
                default: return true;
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleFilter(action: SInputAction) {
        if (IsPressed(action)) {
            filterByType = false;
        } else if (IsReleased(action)) {
            filterByType = true;
        }
        parent.view.refresh();
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_DeactivateFoliageFilter'));
        parent.OnHotkeyHelp(hotkeyList);
    }
    // ------------------------------------------------------------------------
    protected function getFilteredTypes() : array<int> {
        var filter: array<int>;
        var i: int;

        if (filterByType) {
            for (i = 0; i < parent.currentBrushParams.foliage.Size(); i += 1) {
                filter.PushBack(parent.currentBrushParams.foliage[i].typeSlot);
            }
        }
        return filter;
    }
    // ------------------------------------------------------------------------
    public function currentMode() : CName {
        return currentMode;
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnToggleFilter', 'RADFP_DeactivateFoliageFilter');
        theInput.RegisterListener(this, 'OnOperation', 'RADFP_Op1');
        theInput.RegisterListener(this, 'OnOperation', 'RADFP_Op2');
        theInput.RegisterListener(this, 'OnOperation', 'RADFP_Op3');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RADFP_DeactivateFoliageFilter');
        theInput.UnregisterListener(this, 'RADFP_Op1');
        theInput.UnregisterListener(this, 'RADFP_Op2');
        theInput.UnregisterListener(this, 'RADFP_Op3');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state RadFp_InactiveEditor in CRadishFoliageEditor {
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        parent.currentMode = 'Inactive';
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
statemachine class CRadishFoliageEditor extends CEntity {
    // ------------------------------------------------------------------------
    protected var currentMode: CName;
    // ------------------------------------------------------------------------
    protected var intensity: int;

    protected var editedLayer: CRadishFoliageLayer;
    protected var brush: CRadishFoliageBrush;
    protected var currentBrushParams: SRadFoliageBrushDef;
    // ------------------------------------------------------------------------
    protected var foliageSetBrush: CRadishFoliageSetVisualizer;
    // ------------------------------------------------------------------------
    protected var tracker: CRadishTerrainTracker;
    protected var terrainProbe: CRadishCachingTerrainProbe;
    // ------------------------------------------------------------------------
    protected var view: CRadPlanterModeView;
    // ------------------------------------------------------------------------
    protected var cmdActive: bool;
    protected var locked: bool;
    // ------------------------------------------------------------------------
    private var lastState: CName;
    // ------------------------------------------------------------------------
    public function init(
        defaultBrushParams: SRadFoliageBrushDef, tracker: CRadishTerrainTracker)
    {
        GotoState('RadFp_InactiveEditor');
        lastState = 'RadFp_Planting';

        this.tracker = tracker;
        this.terrainProbe = (CRadishCachingTerrainProbe)tracker.getTerrainProbe();

        brush = new CRadishFoliageBrush in this;
        brush.init();

        foliageSetBrush = new CRadishFoliageSetVisualizer in this;
        foliageSetBrush.init(brush, terrainProbe);

        currentBrushParams = defaultBrushParams;
    }
    // ------------------------------------------------------------------------
    public function activate(layer: CRadishFoliageLayer, view: CRadPlanterModeView) {
        this.view = view;

        // id for normal intensity
        intensity = 2;
        brush.show(true);
        // edited layers have to be visible
        layer.show(true);
        this.editedLayer = layer;
        // activate last used mode
        registerListeners();
        GotoState(lastState);
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        var null: CRadishFoliageLayer;

        this.editedLayer = null;
        brush.show(false);
        foliageSetBrush.show(false);

        unregisterListeners();
        // store current mode and unregister
        lastState = GetCurrentStateName();
        GotoState('RadFp_InactiveEditor');
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        foliageSetBrush.destroy();
        // destroy entity
        Destroy();
    }
    // ------------------------------------------------------------------------
    public function opInProgress() : bool {
        return cmdActive;
    }
    // ------------------------------------------------------------------------
    public function cancelOp() {
        if (cmdActive) {
            //LogChannel('DEBUG', "trying to cancel current cmd");
        }
    }
    // ------------------------------------------------------------------------
    public function getBrush() : CRadishFoliageBrush {
        return this.brush;
    }
    // ------------------------------------------------------------------------
    public function getBrushVisualizer() : IRadishPlaceableElement {
        // depending on the current mode use different visualizers
        if (currentMode == 'RADFP_PlacementMode') {
            return this.foliageSetBrush;
        } else {
            return this.brush;
        }
    }
    // ------------------------------------------------------------------------
    public function setBrushParams(params: SRadFoliageBrushDef) {
        currentBrushParams = params;
        view.refresh();
    }
    // ------------------------------------------------------------------------
    public function changeMode(newMode: CName) {
        if (currentMode != newMode) {
            switch (newMode) {
                case 'RADFP_PlantingMode':  GotoState('RadFp_Planting'); break;
                case 'RADFP_PlacementMode': GotoState('RadFp_Placement'); break;
                case 'RADFP_ScalingMode':   GotoState('RadFp_Scaling'); break;
                case 'RADFP_DeletingMode':  GotoState('RadFp_Deleting'); break;
            }
        }
    }
    // ------------------------------------------------------------------------
    public function changeStepSize(type: String) {
        switch (type) {
            case "slow": intensity = 1; break;
            case "fast": intensity = 3; break;
            default:     intensity = 2;
        }
    }
    // ------------------------------------------------------------------------
    event OnChangeBrushSize(action: SInputAction) {
        if (action.value != 0) {
            if (action.value < 0) {
                this.brush.decreaseSize(intensity);
            } else {
                this.brush.increaseSize(intensity);
            }
            view.refresh();
        }
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_AdjustBrushSize'));
    }
    // ------------------------------------------------------------------------
    public function currentMode() : CName {
        return currentMode;
    }
    // ------------------------------------------------------------------------
    public function brushName() : String {
        return currentBrushParams.brushName;
    }
    // ------------------------------------------------------------------------
    public function brushSizeInfo() : String {
        var radius: float;

        radius = brush.getRadius();
        if (brush.getRadius() < 1.0) {
            if (brush.isMinimumSize()) {
                return ".";
            } else {
                return FloatToStringPrec(brush.getRadius() * 2.0, 1);
            }
        } else {
            return FloatToStringPrec(brush.getRadius() * 2.0, 0);
        }
    }
    // ------------------------------------------------------------------------
    event OnCurrentParamInfo(out info: String) {
        info = "";
    }
    // ------------------------------------------------------------------------
    protected function notice(msg: String) {
        theGame.GetGuiManager().ShowNotification(msg);
    }
    // ------------------------------------------------------------------------
    protected function error(errMsg: String) {
        theGame.GetGuiManager().ShowNotification(errMsg);
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        theInput.RegisterListener(this, 'OnChangeBrushSize', 'RADFP_AdjustBrushSize');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        theInput.UnregisterListener(this, 'RADFP_AdjustBrushSize');
    }
    // ------------------------------------------------------------------------
    public function disableCommands() {
        locked = true;
        brush.show(false);
    }
    // ------------------------------------------------------------------------
    public function enableCommands() {
        locked = false;
        brush.show(true);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
