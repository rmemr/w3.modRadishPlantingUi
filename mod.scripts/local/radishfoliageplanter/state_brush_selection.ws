// ----------------------------------------------------------------------------
state RadFp_InteractiveBrushSelection in CRadishFoliagePlanterUi {
    // ------------------------------------------------------------------------
    private var searchFilterInput: bool;
    // ------------------------------------------------------------------------
    private var listProvider: CRadishFoliageBrushList;
    // ------------------------------------------------------------------------
    private var previewBasePos: Vector;
    private var previewFoliage: array<CEntity>;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var camPlacement: SRadishPlacement;

        parent.workContext = 'MOD_RadFp_BrushSelection';
        listProvider = parent.brushManager.getBrushList();

        theInput.StoreContext(parent.workContext);
        registerListeners();

        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        // set cam to a higher grid aligned preview position
        camPlacement = parent.theCam.getSettings();
        camPlacement.pos.Z += 50.0;
        camPlacement.rot = EulerAngles(0.0, 0.0, 0.0);
        parent.theCam.switchTo(camPlacement);
        previewBasePos = camPlacement.pos + Vector(0.0, 17.0, -5.0);

        previewSelectedBrush();

        updateFpCaption();
        parent.showUi(true);
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        despawnPreviewFoliage();
        // restore cam
        parent.theCam.switchTo();

        parent.showUi(false);

        unregisterListeners();
        theInput.RestoreContext(parent.workContext, true);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SelectPrevNextBrush', 'RADFP_SelectPrevNext'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_SetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ResetFilter'));
        hotkeyList.PushBack(HotkeyHelp_from('RADFP_ToggleUi'));
    }
    // ------------------------------------------------------------------------
    // called by user action to start filter input
    event OnFilter(action: SInputAction) {
        if (!parent.view.listMenuRef.isEditActive() && IsPressed(action)) {
            searchFilterInput = true;
            parent.view.listMenuRef.startInputMode(
                GetLocStringByKeyExt("RADFP_lFilter"), listProvider.getWildcardFilter());
        }
    }
    // ------------------------------------------------------------------------
    // called by user action to reset currently set filter
    event OnResetFilter() {
        listProvider.resetWildcardFilter();
        parent.view.listMenuRef.resetEditField();

        updateView();
        searchFilterInput = false;
    }
    // ------------------------------------------------------------------------
    event OnInputEnd(inputString: String) {
        if (searchFilterInput) {
            if (inputString == "") {
                OnResetFilter();
            } else {
                // Note: filter field is not removed to indicate the current filter
                listProvider.setWildcardFilter(inputString);
                updateView();
            }
            searchFilterInput = false;
        }
    }
    // ------------------------------------------------------------------------
    event OnInputCancel() {
        if (searchFilterInput) {
            searchFilterInput = false;
            parent.notice(GetLocStringByKeyExt("UI_CanceledSearch"));
        }
        parent.view.listMenuRef.resetEditField();
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnCycleSelection(action: SInputAction) {
        var prev: bool;
        var id: String;

        if (action.value != 0) {
            if (action.value > 0) {
                id = listProvider.getPreviousId();
            } else {
                id = listProvider.getNextId();
            }
            OnSelected(id);
        }
    }
    // ------------------------------------------------------------------------
    event OnUpdateView() {
        var wildcard: String;
        // Note: if search filter is active show the wildcard to indicate the
        // current filter
        wildcard = listProvider.getWildcardFilter();
        if (wildcard != "") {
            parent.view.listMenuRef.setInputFieldData(
                GetLocStringByKeyExt("RADFP_lFilter"), wildcard);
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    // -- called by listview
    event OnSelected(listItemId: String) {
        // listprovider opens a category if a category was selected otherwise
        // returns true (meaning a "real" item was selected)
        if (listProvider.setSelection(listItemId, true)) {
            parent.foliageEditor.setBrushParams(
              listProvider.getSelectedBrushParams());

            previewSelectedBrush();
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    protected function previewSelectedBrush() {
        var template: CEntityTemplate;
        var entity: CEntity;
        var params: SRadFoliageBrushDef;
        var i, f: int;

        // despawn previous preview foliage
        despawnPreviewFoliage();

        // spawn defined foliage
        params = listProvider.getSelectedBrushParams();
        f = params.foliage.Size();

        for (i = 0; i < f; i += 1) {
            template = parent.foliageManager.getFoliageList().getTemplate(
                params.foliage[i].typeSlot);

            entity = theGame.CreateEntity(template, previewBasePos
                + Vector((f - i) * 2.0));
            entity.AddTag('RADFP_BRUSHPREVIEW');

            previewFoliage.PushBack(entity);
        }
    }
    // ------------------------------------------------------------------------
    protected function despawnPreviewFoliage() {
        var i: int;
        for (i = 0; i < previewFoliage.Size(); i += 1) {
            previewFoliage[i].Destroy();
        }
        previewFoliage.Clear();
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    protected function updateView() {
        // set updated list data and render in listview
        parent.view.listMenuRef.setListData(
            listProvider.getFilteredList(),
            listProvider.getMatchingItemCount(),
            // number of items without filtering
            listProvider.getTotalCount());

        parent.view.listMenuRef.updateView();
    }
    // ------------------------------------------------------------------------
    private function updateFpCaption() {
        parent.view.title = GetLocStringByKeyExt("RADFP_lListBrushOverviewTitle");
        parent.view.statsLabel = GetLocStringByKeyExt("RADFP_lListBrushElements");
        // update field if the menu is already open
        parent.view.listMenuRef.setTitle(GetLocStringByKeyExt("RADFP_lListBrushOverviewTitle"));
        parent.view.listMenuRef.setStatsLabel(GetLocStringByKeyExt("RADFP_lListBrushElements"));
    }
    // ------------------------------------------------------------------------
    event OnCategoryUp(action: SInputAction) {
        if (IsPressed(action)) {
            listProvider.clearLowestSelectedCategory();
            updateView();
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleUi(action: SInputAction) {
        if (IsPressed(action)) {
            parent.toggleUi();
        }
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        if (IsReleased(action)) {
            parent.backToPreviousState(action);
        }
    }
    // ------------------------------------------------------------------------
    private function registerListeners() {
        parent.registerListeners();

        theInput.RegisterListener(this, 'OnBack', 'RAD_Back');

        theInput.RegisterListener(this, 'OnToggleUi', 'RADFP_ToggleUi');
        theInput.RegisterListener(this, 'OnFilter', 'RADFP_SetFilter');
        theInput.RegisterListener(this, 'OnResetFilter', 'RADFP_ResetFilter');
        theInput.RegisterListener(this, 'OnCategoryUp', 'RADFP_ListCategoryUp');

        theInput.RegisterListener(this, 'OnCycleSelection', 'RADFP_SelectPrevNext');
    }
    // ------------------------------------------------------------------------
    private function unregisterListeners() {
        parent.unregisterListeners();

        theInput.UnregisterListener(this, 'RAD_Back');
        theInput.UnregisterListener(this, 'RADFP_ToggleUi');
        theInput.UnregisterListener(this, 'RADFP_SetFilter');
        theInput.UnregisterListener(this, 'RADFP_ResetFilter');
        theInput.UnregisterListener(this, 'RADFP_ListCategoryUp');

        theInput.UnregisterListener(this, 'RADFP_SelectPrevNext');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
