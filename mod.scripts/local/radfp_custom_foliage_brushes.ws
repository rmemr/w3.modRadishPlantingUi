// ----------------------------------------------------------------------------
// Radish Foliange Planter UI: additional foliage brush definitions
//
struct SRadFoliage_CustomBrushDef {
    // required caption for brush (as seen in selection list)
    var brushName: String;
    // optional category to group brushes
    var category: String;
    // defines the planting grid distance in meters: the currently sized brush
    // circle area will be divided into a uniform grid with a tile width of
    // "densityDistance". at most one (randomly selected foliage type) instance
    // will be planted per tile with a randomized position in the tile. there is
    // an additional (interactively adjustable) probability parameter if a
    // foliage instance should be planted in the tile at all to ensure a more
    // random distribution/clumping over the whole brush area.
    // in addition every planting will respect the minimum distance constraints
    // for the foliage type (see below in SRadFoliageBrushParam) for all foliage
    // of the *CURRENTLY EDITED* layer in the current brush area (new and
    // already planted).
    // in short:
    //    o densityDistance small -> more foliage
    //    o densityDistance bigger -> less foliage
    // (valid: [0.05 .. 100.0], default: 4.0)
    var densityDistance: float;
    // set of foliage types with different parameters for picking specific
    // foliage type in current grid tile, see foliage params for probability
    // weighting
    var foliage: array<SRadFoliageBrushParam>;
}
// ----------------------------------------------------------------------------
struct SRadFoliageBrushParam {
    // valid string id for foliage type
    // see repo.quests/all.foliage.repo.yml in radish tools installation dir for
    // valid ids, e.g. abies_break_big
    var typeId: String;
    // defines probability: number of times this foliage will be put into a box
    // to be drawn from.
    // example:
    //      foliage A with weight 3
    //      foliage B with weight 2
    //      foliage C with weight 6
    // => box: A A A B B C C C C C C
    // probabilities to be picked *per grid tile*:
    //      foliage A: 3 / 11 ~ 0.27 = 27 %
    //      foliage B: 2 / 11 ~ 0.18 = 18 %
    //      foliage C: 5 / 11 ~ 0.45 = 45 %
    //
    var weight: byte;
    // constraints for planting this foliage type instance. if the randomly
    // picked position does not respect BOTH minimum distance constraints the
    // planting in current gridtile is SKIPPED
    //
    // defines the minimum distance to surrounding already planted foliage in
    // *CURRENTLY EDITED* foliage layer of the SAME type (based on typeId!)
    // (valid: [0.05 .. 500.0], default: 1.0)
    var minDistanceSameType: Float;
    // defines the minimum distance to surrounding already planted foliage in
    // *CURRENTLY EDITED* foliage layer for ALL OTHER types (based on typeId!)
    // (valid: [0.05 .. 500.0], default: minDistanceSameType)
    var minDistanceOtherType: Float;
    // used range for randomized relative scaling of srt mesh size
    // (valid: [0.2 .. 3.0], default: [0.9 .. 1.1])
    var validScaleMin: Float;
    var validScaleMax: Float;
    // used range for randomized rotation (valid: [0..360], default: [0..360])
    var validRotMin: Float;
    var validRotMax: Float;
}
// ----------------------------------------------------------------------------
// add custom brush definitions here:
// ----------------------------------------------------------------------------
function RADFP_getCustomBrushes() : array<SRadFoliage_CustomBrushDef> {
    var brushes: array<SRadFoliage_CustomBrushDef>;
    var brushDef: SRadFoliage_CustomBrushDef;
    var foliageDef: array<SRadFoliageBrushParam>;

    // ------------------------------------------------------------------------
    // make sure all brushes for a category are added in a continuous sequence
    // otherwise the sub-category display in the selection list will be broken
    // ------------------------------------------------------------------------
    foliageDef.Clear();     // always clear defintions for next brush def
    foliageDef.PushBack(
        SRadFoliageBrushParam(
            "abies_break_stump_slope", 1,   // type, weight
            10, 1,                          // min distances (same, other)
            ,,                              // scale range -> default
            10, 360                         // rotation range
        ));
    // weight 3, distances: 2 (same), 1 (other), default scalerange, default rotation range
    foliageDef.PushBack(SRadFoliageBrushParam("abies_common_big", 2, 2, 1));
    foliageDef.PushBack(SRadFoliageBrushParam("abies_common_bigest", 1, 5, 0.5));
    foliageDef.PushBack(SRadFoliageBrushParam("abies_common_medium", 3, 5, 1));
    // densityDistance 4, category "trees"
    brushes.PushBack(SRadFoliage_CustomBrushDef("example foresty brush", "trees", 4.0, foliageDef));
    // ------------------------------------------------------------------------
    foliageDef.Clear();     // always clear definitions for next brush def
    foliageDef.PushBack(SRadFoliageBrushParam("abies_common_low", 2, 2, 1.5));
    foliageDef.PushBack(SRadFoliageBrushParam("abies_common_lowest", 1, 2, 0.5));
    foliageDef.PushBack(SRadFoliageBrushParam("abies_common_medium", 4, 4, 1.5));

    brushes.PushBack(SRadFoliage_CustomBrushDef("smaller trees foresty brush", "trees", 3.0, foliageDef));
    // ------------------------------------------------------------------------
    foliageDef.Clear();     // always clear definitions for next brush def
    foliageDef.PushBack(SRadFoliageBrushParam("flowers_white_little", 10, 0.25, 0.1));
    foliageDef.PushBack(SRadFoliageBrushParam("flowers_yellow_medium", 7, 0.25, 0.1));
    foliageDef.PushBack(SRadFoliageBrushParam("blowbill", 2, 4, 0.25));
    foliageDef.PushBack(SRadFoliageBrushParam("blowbill_fall", 1, 5, 0.25));

    brushes.PushBack(SRadFoliage_CustomBrushDef("flowers mixed", "flowers", 3, foliageDef));
    // ------------------------------------------------------------------------
    foliageDef.Clear();     // always clear definitions for next brush def
    foliageDef.PushBack(SRadFoliageBrushParam("flowers_white_high", 5, 0.25, 0.1));
    foliageDef.PushBack(SRadFoliageBrushParam("alcea_cluster_violet", 6, 0.2, 0.25));
    foliageDef.PushBack(SRadFoliageBrushParam("alcea_cluster_white", 6, 0.2, 0.25));

    brushes.PushBack(SRadFoliage_CustomBrushDef("big flowers", "flowers", 3, foliageDef));
    // ------------------------------------------------------------------------
    // ...

    return brushes;
}
// ----------------------------------------------------------------------------
// define 4 quick-access brushes
// ----------------------------------------------------------------------------
struct SRadFoliage_BrushQuickAccess {
    var slot: string;
    var brushId: string;
}
// ----------------------------------------------------------------------------
function RADFP_getQuickaccesBrushIds() : array<SRadFoliage_BrushQuickAccess> {
    var quickAccessSlots: array<SRadFoliage_BrushQuickAccess>;

    // only 4 quickaccess slots are defineable (see hotkeys):
    //  RADFP_QuickSelectBrush1
    //  RADFP_QuickSelectBrush2
    //  RADFP_QuickSelectBrush3
    //  RADFP_QuickSelectBrush4
    //  RADFP_QuickSelectBrush5
    // first matching pairing will be used

    // use prefix "CustomBrush_<number>" for custom defined brushes above
    //    <number> defines the slot in the array above (starting with 0)
    quickAccessSlots.PushBack(SRadFoliage_BrushQuickAccess("RADFP_QuickSelectBrush1", "CustomBrush_0"));
    quickAccessSlots.PushBack(SRadFoliage_BrushQuickAccess("RADFP_QuickSelectBrush2", "CustomBrush_1"));
    // use prefix "AutoBrush_<foliagetype>" for autogenerated single foliage brushes
    // see repo.quests/all.foliage.repo.yml in radish tools installation dir for valid <foliagetype>
    quickAccessSlots.PushBack(SRadFoliage_BrushQuickAccess("RADFP_QuickSelectBrush3", "AutoBrush_abies_break_lying"));
    quickAccessSlots.PushBack(SRadFoliage_BrushQuickAccess("RADFP_QuickSelectBrush4", "AutoBrush_abies_break_stump_slope"));
    quickAccessSlots.PushBack(SRadFoliage_BrushQuickAccess("RADFP_QuickSelectBrush5", "AutoBrush_pinus_young_medium"));

    return quickAccessSlots;
}
// ----------------------------------------------------------------------------
